import 'package:flutter/material.dart';

import 'const.dart';

enum TextType { DemiLight, Light, Medium, Bold, Black }

FontWeight getFontWeight(TextType type) {
  switch (type) {
    case TextType.DemiLight:
      return FontWeight.w200;
      break;
    case TextType.Light:
      return FontWeight.w300;
      break;
    case TextType.Medium:
      return FontWeight.w500;
      break;
    case TextType.Bold:
      return FontWeight.w700;
      break;
    case TextType.Black:
      return FontWeight.w900;
      break;
  }
  return FontWeight.w500;
}

class TextG extends StatelessWidget {
  final String text;
  final num size;
  final Color color;
  final TextType type;
  final double height;

  final TextAlign align;
  TextG(this.text,
      {this.size = 16,
      this.color = text_gray,
      this.type = TextType.Medium,
      this.align = TextAlign.center,
      this.height = 1.0});
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: align,
      style: TextStyle(
          height: height,
          color: color,
          fontSize: size.toDouble(),
          fontFamily: "GmarketSans",
          fontWeight: getFontWeight(type)),
    );
  }
}

class TextGU extends StatelessWidget {
  final String text;
  final num size;
  final Color color;
  final TextType type;
  final double height;

  final TextAlign align;
  TextGU(this.text,
      {this.size = 16,
      this.color = text_gray,
      this.type = TextType.Medium,
      this.align = TextAlign.center,
      this.height = 1.0});
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: align,
      style: TextStyle(
          decoration: TextDecoration.underline,
          height: height,
          color: color,
          fontSize: size.toDouble(),
          fontFamily: "GmarketSans",
          fontWeight: getFontWeight(type)),
    );
  }
}

class TextNoto extends StatelessWidget {
  final String text;
  final num size;
  final Color color;
  final TextType type;
  final double height;
  final TextAlign align;

  TextNoto(this.text,
      {this.size = 16,
      this.color = text_gray,
      this.type = TextType.Medium,
      this.align = TextAlign.center,
      this.height = 1.0});
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: align,
      style: TextStyle(
          height: height,
          color: color,
          fontSize: size.toDouble(),
          fontFamily: "NotoSans",
          fontWeight: getFontWeight(type)),
    );
  }
}

class DotText extends StatelessWidget {
  final String text;

  DotText(this.text);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 37, top: 4, bottom: 4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 6), 
            child: SizedBox(
              width: 10,
              height: 10,
              child: Container(
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.black),
              ),
            ),
          ),
          Space(8),
          Container(
            width: MediaQuery.of(context).size.width * 0.8,
            child: TextG(
              text,
              height: 1.6,
              size: 12,
              color: Color(0xff919191),
              align: TextAlign.start,
            ),
          )
        ],
      ),
    );
  }
}
