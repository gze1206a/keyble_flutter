import 'package:flutter/material.dart';
import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:provider/provider.dart';

import 'button.dart';
import 'text.dart';

class BaseScreen extends StatelessWidget {
  BaseScreen(
      {Key key,
      this.title,
      this.appbar,
      this.appbarEvent,
      this.centerTitle,
      this.body,
      this.bottom,
      this.backgroundColor,
      this.useLoading = true,
      this.resizeToAvoidBottomPadding = true,
      this.floatingActionButton})
      : super(key: key);

  final AppBar appbar;
  final VoidCallback appbarEvent;
  final bool resizeToAvoidBottomPadding;
  final bool useLoading;
  final String title;
  final bool centerTitle;
  final Color backgroundColor;
  final Widget body;
  final Widget bottom;
  final FloatingActionButton floatingActionButton;

  @override
  Widget build(BuildContext context) {
    final uiprovider = Provider.of<UIProvider>(context);
    return Scaffold(
      resizeToAvoidBottomPadding: resizeToAvoidBottomPadding,
      backgroundColor: backgroundColor ?? Colors.white,
      appBar: appbar ??
          AppBar(
            title: TextG(
              title ?? "",
              size: 16,
              type: TextType.Bold,
            ),
            centerTitle: centerTitle ?? true,
            automaticallyImplyLeading: false,
            leading: XButton(event: appbarEvent),
            elevation: 0,
          ),
      body: Stack(
        children: <Widget>[
          body,
          Visibility(
            visible: useLoading && uiprovider.isLoading,
            child: Center(
                child: SizedBox(
              width: 40,
              height: 40,
              child: LoadingIndicator(
                indicatorType: Indicator.values[31],
                color: Colors.black,
              ),
            )),
          )
        ],
      ),
      bottomNavigationBar: bottom,
      floatingActionButton: floatingActionButton ?? Container(),
    );
  }
}

class BaseScreenWithoutAppbar extends StatelessWidget {
  BaseScreenWithoutAppbar(
      {Key key,
      this.body,
      this.bottom,
      this.backgroundColor,
      this.useLoading = true,
      this.resizeToAvoidBottomPadding = true,
      this.floatingActionButton})
      : super(key: key);
  final bool useLoading;
  final bool resizeToAvoidBottomPadding;
  final Color backgroundColor;
  final Widget body;
  final Widget bottom;
  final FloatingActionButton floatingActionButton;

  @override
  Widget build(BuildContext context) {
    final uiprovider = Provider.of<UIProvider>(context);
    return Scaffold(
      // resizeToAvoidBottomPadding: resizeToAvoidBottomPadding,
      backgroundColor: backgroundColor ?? Colors.white,
      body: Stack(
        children: <Widget>[
          body,
          Visibility(
            visible: useLoading && uiprovider.isLoading,
            child: Center(
                child: SizedBox(
              width: 40,
              height: 40,
              child: LoadingIndicator(
                indicatorType: Indicator.values[31],
                color: Colors.black,
              ),
            )),
          )
        ],
      ),
      floatingActionButton: floatingActionButton ?? Container(),
    );
  }
}
