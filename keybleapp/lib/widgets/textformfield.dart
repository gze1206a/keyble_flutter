import 'package:flutter/material.dart';

import 'button.dart';
import 'const.dart';

class LoginFormField extends StatelessWidget {
  final String label;
  final TextEditingController controller;
  final FormFieldValidator<String> validator;
  final TextInputType type;
  final String value;
  final bool isPassword;
  final Function(String) onchange;
  LoginFormField(this.label, this.onchange,
      {this.value,
      this.controller,
      this.isPassword = false,
      this.validator,
      this.type = TextInputType.emailAddress});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: value,
      onChanged: onchange,
      controller: controller,
      obscureText: isPassword,
      validator: validator,
      keyboardType: type,
      decoration: new InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
        labelText: label,
        labelStyle:
            TextStyle(color: border_gray, backgroundColor: Colors.transparent),
        enabledBorder: new UnderlineInputBorder(
          borderSide: BorderSide(color: border_gray, width: 1.0),
        ),
        focusedBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: Colors.black12)),
      ),
      style: new TextStyle(
          fontSize: 16, fontFamily: "GmarketSans", fontWeight: FontWeight.w500),
    );
  }
}

class LoginFormFieldWithButton extends StatelessWidget {
  final String label;
  final String buttonlabel;
  final VoidCallback event;
  final TextEditingController controller;
  final FormFieldValidator<String> validator;
  final TextInputType type;
  final String value;
  final bool isPassword;
  final Function(String) onchange;
  LoginFormFieldWithButton(
      this.label, this.onchange, this.buttonlabel, this.event,
      {this.value,
      this.controller,
      this.isPassword = false,
      this.validator,
      this.type = TextInputType.emailAddress});
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        TextFormField(
          initialValue: value,
          onChanged: onchange,
          controller: controller,
          obscureText: isPassword,
          validator: validator,
          keyboardType: type,
          decoration: new InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
            labelText: label,
            labelStyle: TextStyle(
                color: border_gray, backgroundColor: Colors.transparent),
            enabledBorder: new UnderlineInputBorder(
              borderSide: BorderSide(color: border_gray, width: 1.0),
            ),
            focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.black12)),
          ),
          style: new TextStyle(
            fontSize: 16,
            fontFamily: "GmarketSans",
          ),
        ),
        Align(
          alignment: Alignment.centerRight,
          child: OutLButton(buttonlabel, event),
        )
      ],
    );
  }
}
