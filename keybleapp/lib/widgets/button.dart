import 'package:flutter/material.dart';
import 'package:keybleapp/widgets/text.dart';

import 'const.dart';

class XButton extends StatelessWidget {
  final VoidCallback event;
  XButton({this.event});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: event ??
            () {
              Navigator.pop(context);
            },
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.all(20),
          child: Image.asset(
            "assets/images/icon_x.png",
            width: 20,
          ),
        ));
  }
}

class OutLMiniButton extends StatelessWidget {
  final String label;
  final Color color;
  final VoidCallback event;
  OutLMiniButton(this.label, this.event, {this.color});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: event,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
          child: TextG(
            label,
            size: 12,
            color: color ?? Colors.black,
          ),
          decoration: BoxDecoration(
              border: Border.all(color: color ?? Colors.black, width: 1),
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              )),
        ));
  }
}
class OutLMButton extends StatelessWidget {
  final String label;
  final Color color;
  final VoidCallback event;
  OutLMButton(this.label, this.event, {this.color});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: event,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 6),
          child: TextG(
            label,
            size: 12,
            color: color ?? Colors.black,
          ),
          decoration: BoxDecoration(
              border: Border.all(color: color ?? Colors.black, width: 1),
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              )),
        ));
  }
}

class FMButton extends StatelessWidget {
  final String label;
  final Color fontcolor;
  final Color bgcolor;
  final VoidCallback event;
  FMButton(this.label, this.event, {this.fontcolor, this.bgcolor});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: event,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 6),
          child: TextG(
            label,
            size: 12,
            color: fontcolor ?? Colors.white,
          ),
          decoration: BoxDecoration(
              color: bgcolor ?? Colors.black,
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              )),
        ));
  }
}

class OutLButton extends StatelessWidget {
  final String label;
  final Color color;
  final VoidCallback event;
  OutLButton(this.label, this.event, {this.color});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: event,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: TextG(
            label,
            size: 14,
            color: color ?? Colors.black,
          ),
          decoration: BoxDecoration(
              border: Border.all(color: color ?? Colors.black, width: 1.5),
              borderRadius: BorderRadius.all(
                Radius.circular(18),
              )),
        ));
  }
}

class FButton extends StatelessWidget {
  final String label;
  final Color fontcolor;
  final Color bgcolor;
  final VoidCallback event;
  FButton(this.label, this.event, {this.fontcolor, this.bgcolor});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: event,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: TextG(
            label,
            size: 14,
            color: fontcolor ?? Colors.white,
          ),
          decoration: BoxDecoration(
              color: bgcolor ?? Colors.black,
              borderRadius: BorderRadius.all(
                Radius.circular(18),
              )),
        ));
  }
}

class DialogOButton extends StatelessWidget {
  final String label;
  final Color color;
  final VoidCallback event;
  DialogOButton(this.label, this.event, {this.color});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: event,
        child: Container(
          width: 130,
          padding: EdgeInsets.symmetric(vertical: 15),
          child: TextG(
            label,
            size: 16,
            color: color ?? Colors.black,
          ),
          decoration: BoxDecoration(
              border: Border.all(color: color ?? Colors.black, width: 1),
              borderRadius: BorderRadius.all(
                Radius.circular(22),
              )),
        ));
  }
}

class DialogFButton extends StatelessWidget {
  final String label;
  final Color fontcolor;
  final Color bgcolor;
  final VoidCallback event;
  DialogFButton(this.label, this.event, {this.fontcolor, this.bgcolor});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: event,
        child: Container(
          width: 130,
          padding: EdgeInsets.symmetric(vertical: 15),
          child: TextG(
            label,
            size: 16,
            color: fontcolor ?? Colors.white,
          ),
          decoration: BoxDecoration(
              color: bgcolor ?? Colors.black,
              borderRadius: BorderRadius.all(
                Radius.circular(22),
              )),
        ));
  }
}

class BottomButton extends StatelessWidget {
  final String text;
  final VoidCallback event;
  final double padding;
  BottomButton(this.text, this.event, {this.padding});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: event,
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding:
                EdgeInsets.symmetric(horizontal: padding ?? 20, vertical: 0),
            child: Card(
                elevation: 0,
                color: Colors.black,
                shape: StadiumBorder(
                    side: BorderSide(color: Colors.white, width: 1.0)),
                child: Center(
                  child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: TextG(
                        text,
                        color: Colors.white,
                        size: 16,
                        type: TextType.Bold,
                      )),
                )),
          ),
        ));
  }
}

class TermButton extends StatelessWidget {
  final String text;
  final VoidCallback event;
  TermButton(
    this.text,
    this.event,
  );
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset("assets/images/icon_check.png"),
    );
  }
}

class AutoLoginButton extends StatelessWidget {
  final bool value;
  final VoidCallback event;
  AutoLoginButton(this.value, this.event);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Container(
          color: Colors.transparent,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                decoration: new BoxDecoration(
                    shape: BoxShape.circle, color: Colors.black),
                width: 24,
                height: 24,
                padding: EdgeInsets.all(4),
                child: Visibility(
                  visible: value,
                  child: Image.asset(
                    "assets/images/icon_check.png",
                    width: 11,
                  ),
                ),
              ),
              Space(8),
              TextG(
                "Keep me logged in",
                size: 12,
                color: Color(0xff333333),
                align: TextAlign.start,
              ),
            ],
          ),
        ),
        onTap: event);
  }
}
