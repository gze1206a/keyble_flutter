import 'package:flutter/material.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/translations.dart';

class TextDialog extends StatelessWidget {
  final String text;
  TextDialog(this.text);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        elevation: 0,
        titlePadding: EdgeInsets.all(0),
        contentPadding: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        content: Container(
          width: 320,
          height: 188,
          child: Column(
            children: <Widget>[
              Space(40),
              TextG(
                text,
                height: 1.6,
                type: TextType.Bold,
              ),
              Space(30),
              DialogFButton("Close", () {
                Navigator.pop(context);
              })
            ],
          ),
        ));
  }
}

class YNDialog extends StatelessWidget {
  final String text;
  final VoidCallback event1;
  final VoidCallback event2;
  YNDialog(this.text, this.event1, this.event2);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        elevation: 0,
        titlePadding: EdgeInsets.all(0),
        contentPadding: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        content: Container(
          width: 320,
          height: 188,
          child: Column(
            children: <Widget>[
              Space(40),
              TextG(
                text,
                height: 1.6,
                type: TextType.Bold,
              ),
              Space(30),
              Row(
                children: <Widget>[
                  Space(24),
                  DialogOButton(
                      Translations.of(context).trans('false'), event1),
                  Space(5),
                  DialogFButton(Translations.of(context).trans('true'), event2),
                  Space(24),
                ],
              )
            ],
          ),
        ));
  }
}

class DoubleTextDialog extends StatelessWidget {
  final String text1;
  final String text2;
  DoubleTextDialog(this.text1, this.text2);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        elevation: 0,
        titlePadding: EdgeInsets.all(0),
        contentPadding: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        content: Container(
          width: 320,
          height: 240,
          child: Column(
            children: <Widget>[
              Space(40),
              TextG(
                text1,
                size: 15,
                height: 1.6,
                type: TextType.Bold,
              ),
              Space(28),
              TextG(
                text2,
                size: 15,
                height: 1.6,
              ),
              Space(30),
              DialogFButton(Translations.of(context).trans('true'), () {
                Navigator.pop(context);
              }),
            ],
          ),
        ));
  }
}

class ChoiceDialog extends StatelessWidget {
  final String title;
  final String text1;
  final String text2;
  final VoidCallback event1;
  final VoidCallback event2;
  ChoiceDialog(this.title, this.text1, this.text2, this.event1, this.event2);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        elevation: 0,
        titlePadding: EdgeInsets.all(0),
        contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        content: Container(
          width: 320,
          height: 210,
          child: Column(
            children: <Widget>[
              Space(10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Space(60),
                  Expanded(
                    child: Space(1),
                  ),
                  TextG(
                    title,
                    type: TextType.Bold,
                  ),
                  Expanded(
                    child: Space(1),
                  ),
                  XButton()
                ],
              ),
              Divider(
                color: Colors.black.withOpacity(0.6),
                height: 1,
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(vertical: 4),
                onTap: event1,
                title: TextG(
                  text1,
                  size: 14,
                  type: TextType.Medium,
                ),
              ),
              Divider(
                color: Colors.black.withOpacity(0.6),
                height: 1,
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(vertical: 4),
                onTap: event2,
                title: TextG(
                  text2,
                  size: 14,
                  type: TextType.Medium,
                ),
              )
            ],
          ),
        ));
  }
}

class PeopleDialog extends StatelessWidget {
  // final String label;
  // final Color fontcolor;
  // final Color bgcolor;
  // final VoidCallback event;
  // FButton(this.label, this.event, {this.fontcolor, this.bgcolor});
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        elevation: 0,
        titlePadding: EdgeInsets.all(0),
        contentPadding: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        content: Container(
          width: 320,
          height: 285,
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 10, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          XButton(),
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(top: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              width: 54.0,
                              height: 54.0,
                              decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: new DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                          "assets/images/green.png"))))
                        ],
                      ))
                ],
              ),
              Space(14),
              TextG(
                "ego_2_st",
                height: 1.6,
                type: TextType.Bold,
              ),
              Space(16),
              TextG(
                "The key has been used\nsuccessfully!!",
                height: 1.6,
                type: TextType.Bold,
              ),
              Space(30),
              Row(
                children: <Widget>[
                  Space(24),
                  DialogOButton(Translations.of(context).trans('false'), () {}),
                  Space(5),
                  DialogFButton(Translations.of(context).trans('true'), () {}),
                  Space(24),
                ],
              )
            ],
          ),
        ));
  }
}

class KeyDialog extends StatelessWidget {
  final String count;
  final String text;
  KeyDialog(this.count, this.text);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        elevation: 0,
        titlePadding: EdgeInsets.all(0),
        contentPadding: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        content: Container(
          width: 320,
          height: 264,
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 28),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            "assets/images/icon_key.png",
                            height: 68,
                          )
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(top: 38),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          TextG(
                            count,
                            size: 16,
                            color: Colors.white,
                          )
                        ],
                      ))
                ],
              ),
              Space(18),
              TextG(
                text,
                height: 1.6,
                type: TextType.Bold,
              ),
              Space(30),
              Row(
                children: <Widget>[
                  Space(24),
                  DialogOButton(Translations.of(context).trans('false'), () {}),
                  Space(5),
                  DialogFButton(Translations.of(context).trans('true'), () {}),
                  Space(24),
                ],
              )
            ],
          ),
        ));
  }
}

class MaskDialog extends StatelessWidget {
  // final String label;
  // final Color fontcolor;
  final Color color;
  // final VoidCallback event;
  MaskDialog({this.color});
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        elevation: 0,
        titlePadding: EdgeInsets.all(0),
        contentPadding: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        content: Container(
          width: 320,
          height: 285,
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 10, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          XButton(),
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(top: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 80.0,
                            height: 80.0,
                            decoration: BoxDecoration(
                                color: color ?? Colors.black,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(16),
                                )),
                          )
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.only(top: 50),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            "assets/images/icon_keyble.png",
                            height: 56,
                          )
                        ],
                      ))
                ],
              ),
              Space(14),
              TextG(
                "VIP Membership",
                size: 14,
                height: 1.6,
                type: TextType.Bold,
              ),
              Space(16),
              TextG(
                "Really want to subscribe this item?",
                size: 14,
                height: 1.6,
                type: TextType.Bold,
              ),
              Space(30),
              Row(
                children: <Widget>[
                  Space(24),
                  DialogOButton(Translations.of(context).trans('false'), () {}),
                  Space(5),
                  DialogFButton(Translations.of(context).trans('true'), () {}),
                  Space(24),
                ],
              )
            ],
          ),
        ));
  }
}
