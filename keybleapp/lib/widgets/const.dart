import 'package:flutter/material.dart';
import 'package:keybleapp/models/object_item.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/text.dart';

const text_gray = Color(0xff333333);
const border_gray = Color(0xffdadada);
const puple = Color(0xffcb4dff);
const lightsky = Color(0xff00ffd4);

class Space extends StatelessWidget {
  final num size;
  Space(this.size);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: size.toDouble(),
      height: size.toDouble(),
    );
  }
}

class ExpandedSpace extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Space(1),
    );
  }
}

class ExpandedWidth extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
    );
  }
}

class ItemView extends StatelessWidget {
  final ObjectItem item;
  ItemView(this.item);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        children: <Widget>[
          Image.network(
            item.photoThumb,
            height: 90,
          ),
          Space(12),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                item.tags != null
                    ? Container(
                        height: 20,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: item.tags.length,
                          itemBuilder: (BuildContext context, int index) {
                            return OutLMiniButton(item.tags[index], () {});
                          },
                        ),
                      )
                    : Container(),
                Space(8),
                TextG(
                  item.name,
                  size: 14,
                  type: TextType.Bold,
                ),
                Space(8),
                TextG(
                  "Trace ${item.followCount}",
                  size: 12,
                  color: Color(0xff919191),
                ),
                Space(16),
                TextG(
                  item.createdAt.substring(0, 10).replaceAll("-", ". "),
                  size: 12,
                  color: Color(0xff919191),
                ),
              ],
            ),
          ),
          item.following
              ? Image.asset(
                  "assets/images/icon_star_check.png",
                  height: 24,
                )
              : Image.asset(
                  "assets/images/icon_star.png",
                  height: 24,
                )
        ],
      ),
    );
  }
}
