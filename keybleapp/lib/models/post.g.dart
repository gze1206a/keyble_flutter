// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Post _$PostFromJson(Map<String, dynamic> json) {
  return Post(
    json['id'] as num,
    json['type'] as String,
    json['body'] as String,
    json['keywords'] as String,
    json['location'] as String,
    json['hashtag'] as String,
    json['media'] as String,
    json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
    json['isPublic'] as bool,
    json['isLocationPublic'] as bool,
    json['balloonUri'] as String,
    json['likesCount'] as num,
    json['commentsCount'] as num,
    json['hasLike'] as bool,
    json['createdAt'] as String,
    json['updatedAt'] as String,
  );
}

Map<String, dynamic> _$PostToJson(Post instance) => <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'body': instance.body,
      'keywords': instance.keywords,
      'location': instance.location,
      'hashtag': instance.hashtag,
      'media': instance.media,
      'user': instance.user,
      'isPublic': instance.isPublic,
      'isLocationPublic': instance.isLocationPublic,
      'balloonUri': instance.balloonUri,
      'likesCount': instance.likesCount,
      'commentsCount': instance.commentsCount,
      'hasLike': instance.hasLike,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };

UserPost _$UserPostFromJson(Map<String, dynamic> json) {
  return UserPost(
    json['post'] == null
        ? null
        : Post.fromJson(json['post'] as Map<String, dynamic>),
    json['objectItem'] == null
        ? null
        : ObjectItem.fromJson(json['objectItem'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UserPostToJson(UserPost instance) => <String, dynamic>{
      'post': instance.post,
      'objectItem': instance.objectItem,
    };
