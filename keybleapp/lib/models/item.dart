import 'package:json_annotation/json_annotation.dart';
import 'package:keybleapp/models/base_object.dart';

part 'item.g.dart';

@JsonSerializable()
class Item extends BaseDTO {
  num id;
  num price;
  bool isEnable;
  String category;
  String createdAt;
  String updatedAt;
  String name;
  String description;
  String image;
  List<Product> products;
  Item(
    this.id,
    this.price,
    this.isEnable,
    this.category,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.description,
    this.image,
    this.products,
  );
  Item.empty(ResponseHeader header) {
    response = header;
  }
  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);
  Map<String, dynamic> toJson() => _$ItemToJson(this);
}

@JsonSerializable()
class Product extends BaseDTO {
  num id;
  String codename;
  num duration;
  bool isEnable;
  String createdAt;
  String updatedAt;
  num amount;
  String name;
  String image;
  Product(
    this.id,
    this.codename,
    this.duration,
    this.isEnable,
    this.createdAt,
    this.updatedAt,
    this.amount,
    this.name,
    this.image,
  );
  Product.empty(ResponseHeader header) {
    response = header;
  }
  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
