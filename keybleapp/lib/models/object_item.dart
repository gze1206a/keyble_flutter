import 'package:json_annotation/json_annotation.dart';
import 'package:keybleapp/models/base_object.dart';

part 'object_item.g.dart';

@JsonSerializable()
class ObjectItem extends BaseDTO {
  String photoThumb;
  num id;
  String codeName;
  String name;
  String description;
  String photo;
  String backgroundPhoto;
  String createdAt;
  num groupId;
  num holderId;
  Group group;
  List<String> tags;
  bool following;
  num followCount;
  ObjectItem(
      this.photoThumb,
      this.id,
      this.codeName,
      this.name,
      this.description,
      this.photo,
      this.backgroundPhoto,
      this.createdAt,
      this.groupId,
      this.holderId,
      this.group,
      this.tags,
      this.following,
      this.followCount);

  ObjectItem.empty(ResponseHeader header) {
    response = header;
  }
  factory ObjectItem.fromJson(Map<String, dynamic> json) =>
      _$ObjectItemFromJson(json);
  Map<String, dynamic> toJson() => _$ObjectItemToJson(this);
}

@JsonSerializable()
class Group extends BaseDTO {
  num id;
  String codeName;
  String createdAt;

  Group(this.id, this.codeName, this.createdAt);

  Group.empty(ResponseHeader header) {
    response = header;
  }
  factory Group.fromJson(Map<String, dynamic> json) => _$GroupFromJson(json);
  Map<String, dynamic> toJson() => _$GroupToJson(this);
}
