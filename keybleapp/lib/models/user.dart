import 'package:json_annotation/json_annotation.dart';
import 'package:keybleapp/models/base_object.dart';

part 'user.g.dart';

@JsonSerializable()
class User extends BaseDTO {
  bool following;
  num id;
  String nickname;
  String name;
  String email;
  String photo;
  String photoThumb;
  bool hasProfile;
  String birth;
  num nationality;
  bool marketingPushEnable;
  bool notificationPushEnable;
  bool isEnabled;
  String createdAt;
  String updatedAt;
  num point;
  @JsonKey(name: 'access_token')
  String accesstoken;
  String firebaseToken;

  User(
    this.following,
    this.id,
    this.nickname,
    this.name,
    this.email,
    this.photo,
    this.photoThumb,
    this.hasProfile,
    this.birth,
    this.nationality,
    this.marketingPushEnable,
    this.notificationPushEnable,
    this.isEnabled,
    this.createdAt,
    this.updatedAt,
    this.point,
  );

  User.empty(ResponseHeader header) {
    response = header;
  }
  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
