// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'object_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ObjectItem _$ObjectItemFromJson(Map<String, dynamic> json) {
  return ObjectItem(
    json['photoThumb'] as String,
    json['id'] as num,
    json['codeName'] as String,
    json['name'] as String,
    json['description'] as String,
    json['photo'] as String,
    json['backgroundPhoto'] as String,
    json['createdAt'] as String,
    json['groupId'] as num,
    json['holderId'] as num,
    json['group'] == null
        ? null
        : Group.fromJson(json['group'] as Map<String, dynamic>),
    (json['tags'] as List)?.map((e) => e as String)?.toList(),
    json['following'] as bool,
    json['followCount'] as num,
  );
}

Map<String, dynamic> _$ObjectItemToJson(ObjectItem instance) =>
    <String, dynamic>{
      'photoThumb': instance.photoThumb,
      'id': instance.id,
      'codeName': instance.codeName,
      'name': instance.name,
      'description': instance.description,
      'photo': instance.photo,
      'backgroundPhoto': instance.backgroundPhoto,
      'createdAt': instance.createdAt,
      'groupId': instance.groupId,
      'holderId': instance.holderId,
      'group': instance.group,
      'tags': instance.tags,
      'following': instance.following,
      'followCount': instance.followCount,
    };

Group _$GroupFromJson(Map<String, dynamic> json) {
  return Group(
    json['id'] as num,
    json['codeName'] as String,
    json['createdAt'] as String,
  );
}

Map<String, dynamic> _$GroupToJson(Group instance) => <String, dynamic>{
      'id': instance.id,
      'codeName': instance.codeName,
      'createdAt': instance.createdAt,
    };
