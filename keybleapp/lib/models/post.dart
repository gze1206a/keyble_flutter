import 'package:json_annotation/json_annotation.dart';
import 'package:keybleapp/models/base_object.dart';
import 'package:keybleapp/models/object_item.dart';
import 'package:keybleapp/models/user.dart';

part 'post.g.dart';

@JsonSerializable()
class Post extends BaseDTO {
  num id;
  //Text,Paint,Sound,Image,Video
  String type;
  String body;
  String keywords;
  String location;
  String hashtag;
  String media;
  User user;
  bool isPublic;
  bool isLocationPublic;
  String balloonUri;
  num likesCount;
  num commentsCount;
  bool hasLike;
  String createdAt;
  String updatedAt;
//   lat	Float
// 위도 값

//   lng	Float
// 경도 값
  // stickers	Sticker[]
//꾸미기 스티커

  Post(
    this.id,
    this.type,
    this.body,
    this.keywords,
    this.location,
    this.hashtag,
    this.media,
    this.user,
    this.isPublic,
    this.isLocationPublic,
    this.balloonUri,
    this.likesCount,
    this.commentsCount,
    this.hasLike,
    this.createdAt,
    this.updatedAt,
  );
  Post.empty(ResponseHeader header) {
    response = header;
  }
  factory Post.fromJson(Map<String, dynamic> json) => _$PostFromJson(json);
  Map<String, dynamic> toJson() => _$PostToJson(this);
}

@JsonSerializable()
class UserPost extends BaseDTO {
  Post post;
  ObjectItem objectItem;

  UserPost(
    this.post,
    this.objectItem,
  );
  UserPost.empty(ResponseHeader header) {
    response = header;
  }
  factory UserPost.fromJson(Map<String, dynamic> json) =>
      _$UserPostFromJson(json);
  Map<String, dynamic> toJson() => _$UserPostToJson(this);
}
