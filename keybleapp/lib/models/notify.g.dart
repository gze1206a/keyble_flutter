// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notify.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Notifiy _$NotifiyFromJson(Map<String, dynamic> json) {
  return Notifiy(
    json['id'] as num,
    json['type'] as String,
    json['userId'] as num,
    json['senderId'] as num,
    json['postId'] as num,
    json['title'] as String,
    json['message'] as String,
    json['photoThumb'] as String,
    json['createdAt'] as String,
    json['updatedAt'] as String,
  );
}

Map<String, dynamic> _$NotifiyToJson(Notifiy instance) => <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'userId': instance.userId,
      'senderId': instance.senderId,
      'postId': instance.postId,
      'title': instance.title,
      'message': instance.message,
      'photoThumb': instance.photoThumb,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };
