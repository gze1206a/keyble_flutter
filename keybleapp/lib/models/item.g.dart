// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Item _$ItemFromJson(Map<String, dynamic> json) {
  return Item(
    json['id'] as num,
    json['price'] as num,
    json['isEnable'] as bool,
    json['category'] as String,
    json['createdAt'] as String,
    json['updatedAt'] as String,
    json['name'] as String,
    json['description'] as String,
    json['image'] as String,
    (json['products'] as List)
        ?.map((e) =>
            e == null ? null : Product.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ItemToJson(Item instance) => <String, dynamic>{
      'id': instance.id,
      'price': instance.price,
      'isEnable': instance.isEnable,
      'category': instance.category,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'name': instance.name,
      'description': instance.description,
      'image': instance.image,
      'products': instance.products,
    };

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    json['id'] as num,
    json['codename'] as String,
    json['duration'] as num,
    json['isEnable'] as bool,
    json['createdAt'] as String,
    json['updatedAt'] as String,
    json['amount'] as num,
    json['name'] as String,
    json['image'] as String,
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'codename': instance.codename,
      'duration': instance.duration,
      'isEnable': instance.isEnable,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'amount': instance.amount,
      'name': instance.name,
      'image': instance.image,
    };
