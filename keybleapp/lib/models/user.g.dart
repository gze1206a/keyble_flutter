// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    json['following'] as bool,
    json['id'] as num,
    json['nickname'] as String,
    json['name'] as String,
    json['email'] as String,
    json['photo'] as String,
    json['photoThumb'] as String,
    json['hasProfile'] as bool,
    json['birth'] as String,
    json['nationality'] as num,
    json['marketingPushEnable'] as bool,
    json['notificationPushEnable'] as bool,
    json['isEnabled'] as bool,
    json['createdAt'] as String,
    json['updatedAt'] as String,
    json['point'] as num,
  )
    ..accesstoken = json['access_token'] as String
    ..firebaseToken = json['firebaseToken'] as String;
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'following': instance.following,
      'id': instance.id,
      'nickname': instance.nickname,
      'name': instance.name,
      'email': instance.email,
      'photo': instance.photo,
      'photoThumb': instance.photoThumb,
      'hasProfile': instance.hasProfile,
      'birth': instance.birth,
      'nationality': instance.nationality,
      'marketingPushEnable': instance.marketingPushEnable,
      'notificationPushEnable': instance.notificationPushEnable,
      'isEnabled': instance.isEnabled,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'point': instance.point,
      'access_token': instance.accesstoken,
      'firebaseToken': instance.firebaseToken,
    };
