import 'package:json_annotation/json_annotation.dart';
import 'package:keybleapp/models/base_object.dart';

part 'notify.g.dart';

@JsonSerializable()
class Notifiy extends BaseDTO {
  num id;
  String type;
  num userId;

  //(해당 정보가 없으면 -1)
  num senderId;
  num postId;

  String title;
  String message;
  String photoThumb;
  String createdAt;
  String updatedAt;

  Notifiy(
    this.id,
    this.type,
    this.userId,
    this.senderId,
    this.postId,
    this.title,
    this.message,
    this.photoThumb,
    this.createdAt,
    this.updatedAt,
  );

  Notifiy.empty(ResponseHeader header) {
    response = header;
  }
  factory Notifiy.fromJson(Map<String, dynamic> json) =>
      _$NotifiyFromJson(json);
  Map<String, dynamic> toJson() => _$NotifiyToJson(this);
}
