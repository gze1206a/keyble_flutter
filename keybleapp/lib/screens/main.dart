import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/material.dart';
import 'package:keybleapp/examples/uikit.dart';
import 'package:keybleapp/providers.dart/theme.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/translations.dart';
import 'package:provider/provider.dart';

class MainPage extends StatelessWidget {
  printlocale() async {
    String locale = await Devicelocale.currentLocale;
    //en_US
    //ko_KR
    //ja_JP
    print(locale);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeProvider>(context);
    printlocale();
    return BaseScreen(
      title: "초기화면",
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'https://reqres.in/ REST API TEST',
            ),
            RaisedButton(
              child: Text(
                Translations.of(context).trans('click'),
              ),
              onPressed: () {
                // print(PeoPle("morpheus", "leader").toJson().toString());
                // var result =
                //     await Provider.of<ReqResProvider>(context, listen: false)
                //         .createUser(PeoPle("morpheus", "leader"));
                // print(result.id);
                // Provider.of<ReqResProvider>(context, listen: false).loadUser(2);
                // Navigator.push(context,
                //     MaterialPageRoute(builder: (context) => SubPage()));
                // Provider.of<UIProvider>(context, listen: false)
                //     .example
                //     .initLoadButton();
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => UIKit()));
                // Navigator.push(context,
                //     MaterialPageRoute(builder: (context) => StreamPage()));
              },
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          theme.switchTheme();
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
