import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/translations.dart';

class JoinFriendPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final uiprovider = Provider.of<UIProvider>(context);

    return BaseScreen(
        appbar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          actions: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: FButton(Translations.of(context).trans('Jump'), () {}),
            ),
            Space(20)
          ],
        ),
        body: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: <Widget>[
                ExpandedWidth(),
                ExpandedSpace(),
                Image.asset(
                  "assets/images/icon_keyble.png",
                  height: 120,
                ),
                ExpandedSpace(),
                TextG(
                  Translations.of(context).trans('WithFriend'),
                  size: 24,
                  height: 1.4,
                  color: Color(0xff333333),
                  type: TextType.Bold,
                ),
                Space(30),
                TextG(
                  Translations.of(context).trans('CompleteProfile'),
                  size: 14,
                  height: 1.4,
                  color: Color(0xffdadada),
                ),
                ExpandedSpace(),
                BottomButton(
                  Translations.of(context).trans('Invitefriend'),
                  () {
                    uiprovider.controller.nextPage(
                        duration: Duration(milliseconds: 600),
                        curve: Curves.easeInOut);
                  },
                  padding: 0,
                ),
                Space(30),
              ],
            )));
  }
}
