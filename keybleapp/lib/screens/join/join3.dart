import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/textformfield.dart';
import 'package:keybleapp/widgets/translations.dart';

class Join3Page extends StatelessWidget {
  final _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final uiprovider = Provider.of<UIProvider>(context);
    final emailValidator = MultiValidator([
      MinLengthValidator(1,
          errorText: Translations.of(context).trans('String_Checkyouremail')),
      EmailValidator(
          errorText: Translations.of(context).trans('String_Checkyouremail')),
    ]);
    return BaseScreen(
        appbarEvent: () {
          uiprovider.controller.previousPage(
              duration: Duration(milliseconds: 600), curve: Curves.easeInOut);
        },
        resizeToAvoidBottomPadding: false,
        title: "Sign Up",
        body: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Space(40),
                    LoginFormField(
                      Translations.of(context).trans('email'),
                      (str) {
                        uiprovider.join.email = str;
                      },
                      validator: emailValidator,
                    ),
                    GestureDetector(
                      child: Container(
                        height: 100,
                        color: Colors.white,
                      ),
                      onTap: () {
                        FocusScope.of(context).unfocus();
                      },
                    ),
                    LoginFormField(
                      Translations.of(context).trans('password'),
                      (str) {
                        uiprovider.join.password = str;
                      },
                      validator: MinLengthValidator(1,
                          errorText:
                              Translations.of(context).trans('PasswordWrong')),
                    ),
                    Space(40),
                    LoginFormField(
                        Translations.of(context).trans('repassword'), (str) {},
                        validator: (val) {
                      if (val.isEmpty)
                        return Translations.of(context).trans('PasswordWrong');
                      if (val != uiprovider.join.password)
                        return Translations.of(context).trans('PasswordWrong');
                      return null;
                    }),
                    GestureDetector(
                      child: Container(
                        height: MediaQuery.of(context).size.height - 500,
                        color: Colors.white,
                      ),
                      onTap: () {
                        FocusScope.of(context).unfocus();
                      },
                    ),
                    BottomButton(
                      "Sign up",
                      () {
                        uiprovider.controller.nextPage(
                            duration: Duration(milliseconds: 600),
                            curve: Curves.easeInOut);
                        if (_formKey.currentState.validate()) {
                          // uiprovider.controller.nextPage(
                          //     duration: Duration(milliseconds: 300),
                          //     curve: Curves.easeInOut);
                        }
                      },
                      padding: 0,
                    ),
                    Space(30),
                  ],
                ),
              ),
            )));
  }
}
