import 'package:flutter/material.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/translations.dart';

class JoinStartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseScreenWithoutAppbar(
        backgroundColor: Colors.black,
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Space(160),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  TextG(
                    Translations.of(context).trans('WelcomeKEYBLE'),
                    size: 32,
                    color: Colors.white,
                    type: TextType.Bold,
                  ),
                  Padding(
                    padding: EdgeInsetsDirectional.only(bottom: 40),
                    child: Image.asset(
                      "assets/images/logo_picture.png",
                      height: 128,
                    ),
                  )
                ],
              ),
              Space(80),
              TextG(
                Translations.of(context).trans('키블은언어장벽'),
                type: TextType.Bold,
                size: 18,
                color: Colors.white,
                height: 1.6,
                align: TextAlign.start,
              ),
              Space(40),
              TextG(
                Translations.of(context).trans('NewExp'),
                type: TextType.Bold,
                size: 18,
                color: Colors.white,
              ),
              ExpandedSpace(),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                      onTap: () {},
                      child: Container(
                        color: Colors.black,
                        padding:
                            EdgeInsets.symmetric(vertical: 40, horizontal: 40),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            TextG(
                              "Go to Start",
                              size: 14,
                              color: Colors.white,
                              align: TextAlign.start,
                            ),
                            Image.asset(
                              "assets/images/icon_path.png",
                              height: 14,
                            )
                          ],
                        ),
                      )),
                ],
              ),
              ExpandedSpace(),
            ],
          ),
        ));
  }
}
