import 'package:flutter/material.dart';
import 'package:keybleapp/screens/join/join1.dart';
import 'package:keybleapp/screens/join/join2.dart';
import 'package:keybleapp/screens/join/join3.dart';
import 'package:keybleapp/screens/join/join4.dart';
import 'package:keybleapp/screens/join/join_finish.dart';
import 'package:keybleapp/screens/join/join_friend.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';

class JoinPage extends StatelessWidget {
  final controller = PageController();
  @override
  Widget build(BuildContext context) {
    final uiprovider = Provider.of<UIProvider>(context);
    uiprovider.controller = controller;
    return BaseScreenWithoutAppbar(
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: controller,
        children: <Widget>[
          Join1Page(),
          Join2Page(),
          Join3Page(),
          Join4Page(),
          JoinFinishPage(),
          JoinFriendPage()
        ],
      ),
    );
  }
}
