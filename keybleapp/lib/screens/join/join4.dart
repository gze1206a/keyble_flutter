import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/textformfield.dart';
import 'package:keybleapp/widgets/translations.dart';

class Join4Page extends StatelessWidget {
  final _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final uiprovider = Provider.of<UIProvider>(context);

    return BaseScreen(
        appbarEvent: () {
          uiprovider.controller.previousPage(
              duration: Duration(milliseconds: 600), curve: Curves.easeInOut);
        },
        resizeToAvoidBottomPadding: false,
        title: "Sign Up",
        body: Form(
            key: _formKey,
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Space(40),
                      Image.asset(
                        "assets/images/icon_people.png",
                        height: 120,
                      ),
                      Space(40),
                      LoginFormField(
                        Translations.of(context).trans('name'),
                        (str) {
                          uiprovider.join.password = str;
                        },
                      ),
                      Space(40),
                      LoginFormFieldWithButton(
                          Translations.of(context).trans('nickname'), (str) {
                        uiprovider.join.password = str;
                      }, Translations.of(context).trans('Check'), () {}),
                      Space(40),
                      LoginFormFieldWithButton(
                          Translations.of(context).trans('birthday'),
                          (str) {},
                          Translations.of(context).trans('Choose'), () {
                        DatePicker.showDatePicker(
                          context,
                          minDateTime: DateTime.now(),
                          locale: DateTimePickerLocale.ko,
                          onConfirm: (dateTime, List<int> index) {
                            // uiprovider.main.date1 =
                            //     "${dateTime.year}년 ${dateTime.month}월 ${dateTime.day}일";
                            // uiprovider.refresh();
                          },
                          maxDateTime: DateTime.now().add(Duration(days: 180)),
                          dateFormat: 'yyyy년-MMMM-dd일',
                          pickerTheme: DateTimePickerTheme(
                            confirm: TextG("확인"),
                            cancel: Text(
                              '',
                            ),
                          ),
                        );
                      }),
                      Space(40),
                      Row(
                        children: <Widget>[
                          Container(
                            width: 80,
                            child: LoginFormField(
                              Translations.of(context).trans('countrycode'),
                              (str) {
                                uiprovider.join.password = str;
                              },
                              value:
                                  Translations.of(context).trans('countrycode'),
                            ),
                          ),
                          Space(20),
                          Expanded(
                            child: LoginFormField(
                              Translations.of(context).trans('phone'),
                              (str) {
                                uiprovider.join.password = str;
                              },
                            ),
                          )
                        ],
                      ),
                      Space(40),
                      LoginFormFieldWithButton(
                          Translations.of(context).trans('code'), (str) {
                        uiprovider.join.password = str;
                      }, Translations.of(context).trans('Resend'), () {}),
                      Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 40, bottom: 30),
                            child: TextG(
                              Translations.of(context).trans('TermCon'),
                              size: 14,
                              color: Color(0xff333333),
                              align: TextAlign.start,
                            ),
                          ),
                          ExpandedSpace(),
                          Container(
                            padding: EdgeInsets.only(top: 40, bottom: 30),
                            child: TextG(
                              Translations.of(context).trans('PrivacyPolicy'),
                              size: 14,
                              color: Color(0xff333333),
                              align: TextAlign.start,
                            ),
                          ),
                        ],
                      ),
                      BottomButton(
                        "Sign up",
                        () {
                          uiprovider.controller.nextPage(
                              duration: Duration(milliseconds: 600),
                              curve: Curves.easeInOut);
                          if (_formKey.currentState.validate()) {
                            // uiprovider.controller.nextPage(
                            //     duration: Duration(milliseconds: 300),
                            //     curve: Curves.easeInOut);
                          }
                        },
                        padding: 0,
                      ),
                      Space(30),
                    ],
                  ),
                ))));
  }
}
