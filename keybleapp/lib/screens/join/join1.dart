import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/translations.dart';

class Join1Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final uiprovider = Provider.of<UIProvider>(context);
    return BaseScreen(
      title: Translations.of(context).trans('개인정보처리방침'),
      body: ListView(
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.symmetric(horizontal: 20),
        children: <Widget>[
          Space(40),
          TextG(
            Translations.of(context).trans('2020PrivacyPolicy'),
            size: 16,
            type: TextType.Bold,
            height: 1.6,
            align: TextAlign.start,
          ),
          Space(40),
          TextG(
            Translations.of(context).trans('WhatLolem'),
            type: TextType.Bold,
            size: 16,
            align: TextAlign.start,
          ),
          Space(8),
          TextG(
            Translations.of(context).trans('privacy_term'),
            size: 14,
            height: 1.4,
            color: Color(0xff333333),
            align: TextAlign.start,
          ),
          Space(40),
          TextG(
            Translations.of(context).trans('WhereFrom'),
            type: TextType.Bold,
            size: 16,
            align: TextAlign.start,
          ),
          Space(8),
          TextG(
            Translations.of(context).trans('privacy_term'),
            size: 14,
            height: 1.4,
            color: Color(0xff333333),
            align: TextAlign.start,
          ),
          Space(40),
          BottomButton(
            Translations.of(context).trans('Next'),
            () {
              uiprovider.controller.nextPage(
                  duration: Duration(milliseconds: 600),
                  curve: Curves.easeInOut);
            },
            padding: 0,
          ),
          Space(30),
        ],
      ),
    );
  }
}
