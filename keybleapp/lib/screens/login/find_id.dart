import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/dialog.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/textformfield.dart';
import 'package:keybleapp/widgets/translations.dart';

class FindIDPage extends StatelessWidget {
  final _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final uiprovider = Provider.of<UIProvider>(context);

    return BaseScreen(
        appbar: AppBar(
          title: TextG(
            Translations.of(context).trans('FindID'),
            size: 16,
            type: TextType.Bold,
          ),
          centerTitle: true,
          elevation: 0,
        ),
        body: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Space(40),
                    Row(
                      children: <Widget>[
                        Container(
                          width: 80,
                          child: LoginFormField(
                            Translations.of(context).trans('countrycode'),
                            (str) {
                              uiprovider.join.password = str;
                            },
                            value:
                                Translations.of(context).trans('countrycode'),
                          ),
                        ),
                        Space(20),
                        Expanded(
                          child: LoginFormField(
                            Translations.of(context).trans('phone'),
                            (str) {
                              uiprovider.join.password = str;
                            },
                          ),
                        )
                      ],
                    ),
                    Space(40),
                    LoginFormFieldWithButton(
                        Translations.of(context).trans('code'), (str) {
                      uiprovider.join.password = str;
                    }, Translations.of(context).trans('Resend'), () {}),
                    GestureDetector(
                      child: Container(
                        height: MediaQuery.of(context).size.height - 350,
                        color: Colors.white,
                      ),
                      onTap: () {
                        FocusScope.of(context).unfocus();
                      },
                    ),
                    BottomButton(
                      Translations.of(context).trans('Send'),
                      () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return DoubleTextDialog(
                                Translations.of(context)
                                    .trans('String_Checkyouremail'),
                                Translations.of(context)
                                        .trans('String_Showemail') +
                                    "\n[abc@naver.com]",
                              );
                            });
                      },
                      padding: 0,
                    ),
                    Space(30),
                  ],
                ),
              ),
            )));
  }
}
