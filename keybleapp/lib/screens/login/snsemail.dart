import 'dart:io';

import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:keybleapp/screens/login/find_id.dart';
import 'package:keybleapp/screens/login/find_pw.dart';
import 'package:keybleapp/screens/main/main.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/providers.dart/network.dart';
import 'package:keybleapp/providers.dart/root.dart';
import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/dialog.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/textformfield.dart';
import 'package:keybleapp/widgets/translations.dart';

class EmailLoginPage extends StatelessWidget {
  final _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final uiprovider = Provider.of<UIProvider>(context);
    final root = Provider.of<RootProvider>(context);
    final emailValidator = MultiValidator([
      MinLengthValidator(1,
          errorText: Translations.of(context).trans('String_Checkyouremail')),
      EmailValidator(
          errorText: Translations.of(context).trans('String_Checkyouremail')),
    ]);
    return BaseScreen(
        resizeToAvoidBottomPadding: false,
        appbar: AppBar(
          title: TextG(
            Translations.of(context).trans('EmailLogin'),
            size: 16,
            type: TextType.Bold,
          ),
          centerTitle: true,
          automaticallyImplyLeading: false,
          elevation: 0,
        ),
        body: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  Space(40),
                  LoginFormField(
                    Translations.of(context).trans('email'),
                    (str) {
                      uiprovider.join.email = str;
                    },
                    validator: emailValidator,
                  ),
                  GestureDetector(
                    child: Container(
                      height: 20,
                      color: Colors.white,
                    ),
                    onTap: () {
                      FocusScope.of(context).unfocus();
                    },
                  ),
                  LoginFormField(
                    Translations.of(context).trans('password'),
                    (str) {
                      uiprovider.join.password = str;
                    },
                    validator: MinLengthValidator(1,
                        errorText:
                            Translations.of(context).trans('PasswordWrong')),
                    isPassword: true,
                  ),
                  Row(
                    children: <Widget>[
                      AutoLoginButton(true, () {}),
                      ExpandedSpace(),
                      GestureDetector(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return ChoiceDialog(
                                    Translations.of(context)
                                        .trans('SelectType'),
                                    Translations.of(context).trans('email'),
                                    Translations.of(context).trans('password'),
                                    () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => FindIDPage()));
                                }, () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => FindPWPage()));
                                });
                              });
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 40),
                          child: TextGU(
                            "Forgot your ID / PW?",
                            size: 12,
                            color: Color(0xff333333),
                            align: TextAlign.start,
                          ),
                        ),
                      )
                    ],
                  ),
                  GestureDetector(
                    child: Container(
                      height: MediaQuery.of(context).size.height - 430,
                      color: Colors.white,
                    ),
                    onTap: () {
                      FocusScope.of(context).unfocus();
                    },
                  ),
                  BottomButton(
                    Translations.of(context).trans('로그인'),
                    () async {
                      uiprovider.onLoad();
                      if (uiprovider.join.email == null ||
                          uiprovider.join.email.isEmpty ||
                          uiprovider.join.password == null ||
                          uiprovider.join.password.isEmpty) {
                        await Provider.of<NetworkProvider>(context,
                                listen: false)
                            .login("aiara01@aiaracorp.com", "aiara8282",
                                Platform.isAndroid ? "android" : "ios");
                        await Provider.of<NetworkProvider>(context,
                                listen: false)
                            .loadObjectItems(root.user.accesstoken);
                        uiprovider.onHide();
                        uiprovider.navigationButtonIndex = 0;
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MainScreen()));
                      } else {
                        await Provider.of<NetworkProvider>(context,
                                listen: false)
                            .login(
                                uiprovider.join.email,
                                uiprovider.join.password,
                                Platform.isAndroid ? "android" : "ios");
                        await Provider.of<NetworkProvider>(context,
                                listen: false)
                            .loadObjectItems(root.user.accesstoken);
                        // await Provider.of<NetworkProvider>(context,
                        //         listen: false)
                        //     .loadItems(root.user.accesstoken);

                        uiprovider.onHide();
                        uiprovider.navigationButtonIndex = 0;
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MainScreen()));
                      }
                    },
                    padding: 0,
                  ),
                  Space(30),
                ],
              ),
            )));
  }
}
