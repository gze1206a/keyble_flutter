import 'package:flutter/material.dart';
import 'package:keybleapp/screens/join/join.dart';
import 'package:keybleapp/screens/login/snsemail.dart';
import 'package:keybleapp/viewmodels/join.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/translations.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/providers.dart/ui.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final uiprovider = Provider.of<UIProvider>(context);
    Widget _buildLoginButton() {
      return GestureDetector(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => JoinPage()));
        },
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 40, vertical: 0),
            child: Card(
                elevation: 0,
                color: Colors.white,
                shape: StadiumBorder(
                    side: BorderSide(color: Colors.white, width: 1.0)),
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextG(
                        Translations.of(context).trans('keyblejoin'),
                        size: 16,
                      )
                    ],
                  ),
                )),
          ),
        ),
      );
    }

    Widget _buildSNSButton() {
      return GestureDetector(
        onTap: () {
          uiprovider.join = Join();
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => EmailLoginPage()));
        },
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 40, vertical: 0),
            child: Card(
                elevation: 0,
                color: Colors.transparent,
                shape: StadiumBorder(
                    side: BorderSide(color: Colors.white, width: 1.0)),
                child: Center(
                  child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: TextG(
                        Translations.of(context).trans('Singsns'),
                        color: Colors.white,
                        size: 16,
                      )),
                )),
          ),
        ),
      );
    }

    return BaseScreenWithoutAppbar(
      backgroundColor: Colors.black,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          ExpandedSpace(),
          Image.asset(
            "assets/images/logo_picture.png",
            height: 128,
          ),
          Space(20),
          Image.asset(
            "assets/images/logo_word.png",
            height: 20,
          ),
          ExpandedSpace(),
          TextG(
            Translations.of(context).trans('계속하려면KEYBLE계정이필요합니다'),
            color: Colors.white,
            size: 16,
            height: 1.6,
          ),
          Space(40),
          _buildLoginButton(),
          Space(11),
          _buildSNSButton(),
          Space(20),
          TextG("ver0.0.01",
              color: Colors.white, size: 14, type: TextType.Light),
          Space(20),
          TextG(
            Translations.of(context).trans('게스트로시작하기'),
            color: Colors.white,
            size: 14,
          ),
          Space(20),
          // Space(MediaQuery.of(context).size.height * 0.1)
        ],
      ),
    );
  }
}
