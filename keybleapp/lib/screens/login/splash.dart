import 'dart:async';

import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/material.dart';
import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/screens/login/login.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void navigationPage() {
    //Navigator.pushReplacement(context, SlideLeftRoute(page: LoginPage()));

    Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginPage()));
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  startTime() async {
    var _duration = new Duration(milliseconds: 100);
    return new Timer(_duration, navigationPage);
  }

  @override
  Widget build(BuildContext context) {
    var uiprovider = Provider.of<UIProvider>(context);
    getlocale() async {
      uiprovider.locale = await Devicelocale.currentLocale;
      //en_US
      //ko_KR
      //ja_JP
    }

    getlocale();
    return BaseScreenWithoutAppbar(
      backgroundColor: Colors.black,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width,
          ),
          Expanded(
            child: Space(1),
          ),
          Image.asset(
            "assets/images/logo_picture.png",
            height: 128,
          ),
          Space(20),
          Image.asset(
            "assets/images/logo_word.png",
            height: 20,
          ),
          Expanded(
            child: Space(1),
          ),
        ],
      ),
    );
  }
}
