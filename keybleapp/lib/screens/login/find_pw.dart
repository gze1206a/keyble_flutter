import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/dialog.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/textformfield.dart';
import 'package:keybleapp/widgets/translations.dart';

class FindPWPage extends StatelessWidget {
  final _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final uiprovider = Provider.of<UIProvider>(context);
    final emailValidator = MultiValidator([
      MinLengthValidator(1,
          errorText: Translations.of(context).trans('String_Checkyouremail')),
      EmailValidator(
          errorText: Translations.of(context).trans('String_Checkyouremail')),
    ]);
    return BaseScreen(
        appbar: AppBar(
          title: TextG(
            // Translations.of(context).trans('EmailLogin'),
            "Forgot Password",
            size: 16,
            type: TextType.Bold,
          ),
          centerTitle: true,
          elevation: 0,
        ),
        body: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Space(40),
                    LoginFormField(
                      Translations.of(context).trans('email'),
                      (str) {
                        uiprovider.join.email = str;
                      },
                      validator: emailValidator,
                    ),
                    GestureDetector(
                      child: Container(
                        height: MediaQuery.of(context).size.height - 270,
                        color: Colors.white,
                      ),
                      onTap: () {
                        FocusScope.of(context).unfocus();
                      },
                    ),
                    BottomButton(
                      Translations.of(context).trans('Send'),
                      () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return DoubleTextDialog(
                                  Translations.of(context)
                                      .trans('String_Checkyouremail'),
                                  Translations.of(context).trans(
                                      'String_Changepasswordtoyouremail'));
                            });
                      },
                      padding: 0,
                    ),
                    Space(30),
                  ],
                ),
              ),
            )));
  }
}
