import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/animation/fadein.dart';
import 'package:keybleapp/providers.dart/root.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/translations.dart';

class SettingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<RootProvider>(context).user;
    Widget _buildDivide() {
      return Divider(
        height: 1,
        thickness: 1,
        color: Colors.black.withOpacity(0.05),
      );
    }

    Widget _buildList(String title, VoidCallback event) {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 28, vertical: 26),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextG(
              title,
              size: 14,
            ),
            ExpandedSpace(),
            TextG(
              ">",
              size: 18,
              type: TextType.Bold,
            ),
          ],
        ),
      );
    }

    return BaseScreen(
      appbar: AppBar(
        title: TextG(
          Translations.of(context).trans('settings'),
          size: 16,
          type: TextType.Bold,
        ),
        centerTitle: true,
        elevation: 0,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                FadeInY(0, _buildDivide()),
                FadeInY(
                    0.25,
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 28, vertical: 26),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TextG(
                            user.email,
                            size: 14,
                            type: TextType.Bold,
                          ),
                          ExpandedSpace(),
                          TextG(
                            ">",
                            size: 18,
                            type: TextType.Bold,
                          ),
                        ],
                      ),
                    )),
                FadeInY(0.25, _buildDivide()),
                FadeInY(
                    0.5,
                    _buildList(
                        Translations.of(context).trans('String_Auto Sign in'),
                        () {})),
                FadeInY(0.5, _buildDivide()),
                FadeInY(
                    0.75,
                    _buildList(
                        Translations.of(context).trans('String_PUSH Message'),
                        () {})),
                FadeInY(0.75, _buildDivide()),
                FadeInY(
                    1,
                    _buildList(
                        Translations.of(context).trans('String_Language'),
                        () {})),
                FadeInY(1, _buildDivide()),
                FadeInY(
                    1.25,
                    _buildList(
                        Translations.of(context)
                            .trans('String_Purchase History'),
                        () {})),
                FadeInY(1.25, _buildDivide()),
                FadeInY(
                    1.5,
                    _buildList(
                        Translations.of(context)
                            .trans('String_Community Guide'),
                        () {})),
                FadeInY(1.5, _buildDivide()),
                FadeInY(
                    1.75,
                    _buildList(
                        Translations.of(context).trans('String_Privacy Policy'),
                        () {})),
                FadeInY(1.75, _buildDivide()),
                FadeInY(
                    2,
                    _buildList(
                        Translations.of(context).trans('String_App Version'),
                        () {})),
                FadeInY(2, _buildDivide()),
              ],
            ),
          ),
          Space(20),
          FadeInY(
            2.25,
            TextG(
              Translations.of(context).trans('String_LogOut'),
              size: 14,
              color: Color(0xff919191),
              type: TextType.Bold,
            ),
          ),
          Space(30),
        ],
      ),
    );
  }
}
