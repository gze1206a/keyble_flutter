import 'package:flutter/material.dart';
import 'package:keybleapp/animation/fadein.dart';
import 'package:keybleapp/models/object_item.dart';
import 'package:keybleapp/providers.dart/root.dart';
import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/screens/main/searchpage.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/translations.dart';
import 'package:provider/provider.dart';

class Main1Screen extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Main1Screen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  UIProvider uiprovider;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      uiprovider.setTabIndex(_tabController.index == 0); // setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final items = Provider.of<RootProvider>(context).objectitems;
    uiprovider = Provider.of<UIProvider>(context);
    Widget _buildItemView(ObjectItem item) {
      return Padding(
        padding: EdgeInsets.all(4),
        child: Stack(
          children: <Widget>[
            Image.network(item.photoThumb),
            Container(
              padding: EdgeInsets.all(48),
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                  borderRadius: BorderRadius.all(
                    Radius.circular(18),
                  )),
              child: Image.asset("assets/images/icon_lock.png"),
            ),
            Positioned(
              bottom: 10,
              right: 10,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      "assets/images/icon_hart.png",
                      height: 12,
                    ),
                    Space(8),
                    TextG(
                      "${item.followCount}",
                      size: 12,
                      color: Colors.white,
                      type: TextType.Bold,
                    )
                  ],
                ),
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.5),
                    borderRadius: BorderRadius.all(
                      Radius.circular(18),
                    )),
              ),
            )
          ],
        ),
      );
    }

    return BaseScreenWithoutAppbar(
      useLoading: false,
      body: Column(
        children: <Widget>[
          Space(12),
          Row(
            children: <Widget>[
              GestureDetector(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Image.asset(
                    "assets/images/icon_notify.png",
                    height: 24,
                  ),
                ),
              ),
              uiprovider.locale == "ko_KR"
                  ? Space(MediaQuery.of(context).size.width * 0.12)
                  : Space(MediaQuery.of(context).size.width * 0.06),
              Expanded(
                child: TabBar(
                  labelPadding: EdgeInsets.zero,
                  indicatorPadding: EdgeInsets.zero,
                  indicatorSize: TabBarIndicatorSize.label,
                  indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      color: Colors.black),
                  controller: _tabController,
                  tabs: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(12, 10, 12, 8),
                      child: TextG(
                        Translations.of(context).trans('Recommend'),
                        size: uiprovider.locale == "ko_KR" ? 14 : 12,
                        color: uiprovider.mainTabIndex
                            ? Colors.white
                            : Colors.black,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(12, 10, 12, 8),
                      child: TextG(
                        Translations.of(context).trans('Follower'),
                        size: uiprovider.locale == "ko_KR" ? 14 : 12,
                        color: uiprovider.mainTabIndex
                            ? Colors.black
                            : Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
              uiprovider.locale == "ko_KR"
                  ? Space(MediaQuery.of(context).size.width * 0.12)
                  : Space(MediaQuery.of(context).size.width * 0.06),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SearchPage()));
                },
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Image.asset(
                    "assets/images/icon_search.png",
                    height: 24,
                  ),
                ),
              )
            ],
          ),
          Expanded(
            child: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                controller: _tabController,
                children: [
                  GridView.count(
                    physics: BouncingScrollPhysics(),
                    padding: EdgeInsets.zero,
                    crossAxisCount: 2,
                    children: List.generate(items.length, (index) {
                      return FadeInY(index * 0.2, _buildItemView(items[index]));
                    }),
                  ),
                  GridView.count(
                    physics: BouncingScrollPhysics(),
                    padding: EdgeInsets.zero,
                    crossAxisCount: 2,
                    children: List.generate(items.length, (index) {
                      return FadeInY(index * 0.2, _buildItemView(items[index]));
                    }),
                  ),
                ]),
          )
        ],
      ),
    );
  }
}
