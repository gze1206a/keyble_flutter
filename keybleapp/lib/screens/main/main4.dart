import 'package:flutter/material.dart';
import 'package:keybleapp/animation/fadein.dart';
import 'package:keybleapp/models/post.dart';
import 'package:keybleapp/screens/setting/setting.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/providers.dart/root.dart';
import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/translations.dart';

class Main4Screen extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Main4Screen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  UIProvider uiprovider;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      uiprovider.setMyTabIndex(_tabController.index == 0); // setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final root = Provider.of<RootProvider>(context);
    uiprovider = Provider.of<UIProvider>(context);

    Widget _buildPostView(UserPost item) {
      return Padding(
        padding: EdgeInsets.all(4),
        child: Stack(
          children: <Widget>[
            Image.network(item.objectItem.photoThumb),
            Container(
              padding: EdgeInsets.all(48),
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                  borderRadius: BorderRadius.all(
                    Radius.circular(18),
                  )),
              child: Image.asset("assets/images/icon_lock.png"),
            ),
            Positioned(
              bottom: 10,
              right: 10,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      "assets/images/icon_hart.png",
                      height: 12,
                    ),
                    Space(8),
                    TextG(
                      "${item.objectItem.followCount}",
                      size: 12,
                      color: Colors.white,
                      type: TextType.Bold,
                    )
                  ],
                ),
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.5),
                    borderRadius: BorderRadius.all(
                      Radius.circular(18),
                    )),
              ),
            )
          ],
        ),
      );
    }

    return BaseScreen(
        useLoading: false,
        appbar: AppBar(
          backgroundColor: Colors.black,
          title: TextG(
            // Translations.of(context).trans('EmailLogin'),
            "Lee",
            size: 16,
            color: Colors.white,
            type: TextType.Bold,
          ),
          actions: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SettingPage()));
              },
              child: Container(
                  color: Colors.black,
                  padding: EdgeInsets.all(16),
                  child: Image.asset(
                    "assets/images/icon_setting.png",
                  )),
            )
          ],
          centerTitle: true,
          automaticallyImplyLeading: false,
          elevation: 0,
        ),
        body: Padding(
          padding: EdgeInsets.only(bottom: 10),
          child: CustomScrollView(
            // CustomScrollView는 children이 아닌 slivers를 사용하며, slivers에는 스크롤이 가능한 위젯이나 리스트가 등록가능함
            slivers: <Widget>[
              // 앱바 추가
              SliverAppBar(
                backgroundColor: Colors.black,
                flexibleSpace: Column(
                  children: <Widget>[
                    Space(20),
                    Container(
                        color: Colors.black,
                        padding: EdgeInsets.symmetric(
                          horizontal: 20,
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                root.user.photoThumb == null
                                    ? Image.asset(
                                        "assets/images/green.png",
                                        height: 80,
                                      )
                                    : Image.network(
                                        root.user.photoThumb,
                                        height: 80,
                                      ),
                                Space(10),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          height: 24,
                                          width: 24,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Color(0xff616161),
                                                  width: 1),
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(6),
                                              )),
                                        ),
                                        Space(8),
                                        Container(
                                          height: 24,
                                          width: 24,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Color(0xff616161),
                                                  width: 1),
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(6),
                                              )),
                                        ),
                                      ],
                                    ),
                                    Space(10),
                                    TextG(
                                      root.user.email,
                                      size: 16,
                                      color: Colors.white,
                                      type: TextType.Bold,
                                    ),
                                    Space(16),
                                    Row(
                                      children: <Widget>[
                                        TextG(
                                          "45",
                                          size: 14,
                                          color: Color(0xff969696),
                                          type: TextType.Bold,
                                        ),
                                        Space(5),
                                        TextG(
                                          "Friends",
                                          size: 14,
                                          color: Color(0xff969696),
                                        ),
                                        Space(20),
                                        TextG(
                                          "46",
                                          size: 14,
                                          color: Color(0xff969696),
                                          type: TextType.Bold,
                                        ),
                                        Space(5),
                                        TextG(
                                          "Favorite",
                                          size: 14,
                                          color: Color(0xff969696),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ],
                            ),
                            Space(32),
                            Container(
                              //  width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.symmetric(
                                  vertical: 20, horizontal: 40),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      TextG(
                                        Translations.of(context)
                                            .trans('Mycoin'),
                                        size: 14,
                                        color: Colors.white,
                                        type: TextType.Bold,
                                      ),
                                      Space(10),
                                      TextG(
                                        "4,400",
                                        size: 16,
                                        color: Colors.white,
                                        type: TextType.Bold,
                                      ),
                                    ],
                                  ),
                                  Container(
                                    width: 1,
                                    height: 32,
                                    color: Color(0xff979797).withOpacity(0.5),
                                  ),
                                  Column(
                                    children: <Widget>[
                                      TextG(
                                        "My Item",
                                        size: 14,
                                        color: Colors.white,
                                        type: TextType.Bold,
                                      ),
                                      Space(10),
                                      TextG(
                                        "12",
                                        size: 16,
                                        color: Colors.white,
                                        type: TextType.Bold,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                  color: Color(0xffdfdfdf).withOpacity(0.2),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(32),
                                  )),
                            ),
                            Space(20)
                          ],
                        )),
                  ],
                ),
                // 최대 높이
                pinned: false,
                expandedHeight: 260,
              ),
              SliverAppBar(
                title: TabBar(
                  indicatorColor: Colors.black,
                  controller: _tabController,
                  tabs: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: TextG(
                        Translations.of(context).trans('MyPage_Collection'),
                        size: 14,
                        color: uiprovider.myTabIndex
                            ? Colors.black
                            : Color(0xffdcdcdc),
                        type: TextType.Bold,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: TextG(
                        Translations.of(context).trans('Trace'),
                        size: 14,
                        color: uiprovider.myTabIndex
                            ? Color(0xffdcdcdc)
                            : Colors.black,
                        type: TextType.Bold,
                      ),
                    ),
                  ],
                ),
                pinned: true,
                automaticallyImplyLeading: false,
              ),

              uiprovider.myTabIndex
                  ? SliverList(
                      delegate: SliverChildListDelegate(
                      List.generate(
                          root.collections.length < 7
                              ? 7
                              : root.collections.length, (index) {
                        print("$index ${root.collections.length}");
                        if (index >= root.collections.length - 1)
                          return Container(
                            height: 80,
                          );
                        return FadeInY(
                            index * 0.2, ItemView(root.collections[index]));
                      }),
                    ))
                  : SliverGrid(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 5.0,
                        mainAxisSpacing: 5.0,
                      ),
                      delegate: SliverChildListDelegate(
                        List.generate(root.posts.length, (index) {
                          return FadeInY(
                              0.2 * index, _buildPostView(root.posts[index]));
                        }),
                      ),
                    )
            ],
          ),
        ));
  }
}
