import 'package:flutter/material.dart';
import 'package:keybleapp/models/object_item.dart';
import 'package:keybleapp/providers.dart/root.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:provider/provider.dart';

class SearchBarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final items = Provider.of<RootProvider>(context).objectitems;
    Widget _buildTitle(String str) {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
          child: Row(
            children: <Widget>[
              TextG(
                str,
                size: 14,
                type: TextType.Bold,
              ),
            ],
          ));
    }

    Widget _buildItemView(ObjectItem item) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Row(
          children: <Widget>[
            Image.network(
              item.photoThumb,
              height: 90,
            ),
            Space(12),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  item.tags != null
                      ? Container(
                          height: 20,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: item.tags.length,
                            itemBuilder: (BuildContext context, int index) {
                              return OutLMiniButton(item.tags[index], () {});
                            },
                          ),
                        )
                      : Container(),
                  Space(8),
                  TextG(
                    item.name,
                    size: 14,
                    type: TextType.Bold,
                  ),
                  Space(8),
                  TextG(
                    "Trace ${item.followCount}",
                    size: 12,
                    color: Color(0xff919191),
                  ),
                  Space(16),
                  TextG(
                    item.createdAt.substring(0, 10).replaceAll("-", ". "),
                    size: 12,
                    color: Color(0xff919191),
                  ),
                ],
              ),
            ),
            item.following
                ? Image.asset(
                    "assets/images/icon_star_check.png",
                    height: 24,
                  )
                : Image.asset(
                    "assets/images/icon_star.png",
                    height: 24,
                  )
          ],
        ),
      );
    }

    return BaseScreenWithoutAppbar(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 36),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 14, vertical: 0),
                    child: Image.asset(
                      "assets/images/icon_search_2.png",
                      height: 20,
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      onSubmitted: (str) {},
                      //controller: searchController,
                      autofocus: true,
                      decoration: InputDecoration(
                        hintText: "Search",
                        border: InputBorder.none,
                        hintStyle: TextStyle(color: Colors.black26),
                      ),
                      style: TextStyle(color: Colors.black, fontSize: 16.0),
                      //   onChanged: (query) => updateSearchQuery,
                    ),
                  ),
                  Opacity(
                    opacity: 0.2,
                    child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          color: Colors.transparent,
                          padding:
                              EdgeInsets.symmetric(horizontal: 16, vertical: 0),
                          child: Image.asset(
                            "assets/images/icon_x.png",
                            width: 14,
                          ),
                        )),
                  )
                ],
              ),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.black.withOpacity(0.1), width: 1.5),
                  borderRadius: BorderRadius.all(
                    Radius.circular(24),
                  )),
            ),
          ),
          Space(22),
        ],
      ),
    );
  }
}
