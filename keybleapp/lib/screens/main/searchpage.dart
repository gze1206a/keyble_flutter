import 'package:flutter/material.dart';
import 'package:keybleapp/animation/fadein.dart';
import 'package:keybleapp/models/object_item.dart';
import 'package:keybleapp/providers.dart/root.dart';
import 'package:keybleapp/screens/main/searchbar.dart';
import 'package:keybleapp/transitions/slide_route.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:provider/provider.dart';
import 'package:slide_popup_dialog/slide_popup_dialog.dart' as slideDialog;

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final items = Provider.of<RootProvider>(context).objectitems;
    Widget _buildTitle(String str) {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
          child: Row(
            children: <Widget>[
              TextG(
                str,
                size: 14,
                type: TextType.Bold,
              ),
            ],
          ));
    }

    Widget buildSlidePanel(ObjectItem item) {
      return Container(
        height: MediaQuery.of(context).size.height * 0.6,
        child: Column(
          children: <Widget>[
            Image.network(
              item.photoThumb,
              height: 90,
            ),
            Space(24),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                item.following
                    ? Image.asset(
                        "assets/images/icon_star_check.png",
                        height: 24,
                      )
                    : Image.asset(
                        "assets/images/icon_star.png",
                        height: 24,
                      ),
                Space(8),
                TextG(
                  item.name,
                  size: 18,
                  type: TextType.Bold,
                ),
              ],
            ),
            Space(24),
            item.tags != null
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(
                      item.tags.length,
                      (index) {
                        return OutLMButton(item.tags[index], () {});
                      },
                    ),
                  )
                : Container(),
            Space(24),
            Divider(
              color: Color(0xffdadada),
              thickness: 1,
              height: 1,
            ),
            Space(20),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextG(
                  item.description,
                  size: 14,
                  height: 1.4,
                  color: Color(0xff919191),
                  align: TextAlign.center,
                ),
              ),
            ),
            BottomButton("Enter into this world", () {})
          ],
        ),
      );
    }

    return BaseScreen(
      title: "Things Book",
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.push(context, SlideTopRoute(page: SearchBarPage()));
            },
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Container(
                height: 40,
                padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 14, vertical: 0),
                      child: Image.asset(
                        "assets/images/icon_search_2.png",
                        height: 20,
                      ),
                    ),
                    ExpandedSpace(),
                    Opacity(
                      opacity: 0.2,
                      child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            color: Colors.transparent,
                            padding: EdgeInsets.symmetric(
                                horizontal: 16, vertical: 0),
                            child: Image.asset(
                              "assets/images/icon_x.png",
                              width: 14,
                            ),
                          )),
                    )
                  ],
                ),
                decoration: BoxDecoration(
                    border: Border.all(
                        color: Colors.black.withOpacity(0.1), width: 1.5),
                    borderRadius: BorderRadius.all(
                      Radius.circular(24),
                    )),
              ),
            ),
          ),
          Space(22),
          Expanded(
            child: ListView(
              children: <Widget>[
                _buildTitle("Recommand tag"),
                Space(16),
                Row(
                  children: <Widget>[
                    Space(24),
                    FButton("#Airmax", () {}),
                    Space(4),
                    FButton("#97", () {}),
                    Space(4),
                    FButton("#19FW", () {}),
                  ],
                ),
                Space(20),
                _buildTitle("All things"),
                Column(
                  children: List.generate(items.length, (index) {
                    return GestureDetector(
                      onTap: () {
                        slideDialog.showSlideDialog(
                          context: context,
                          child: buildSlidePanel(items[index]),
                          barrierColor: Colors.black.withOpacity(0.6),
                          pillColor: Colors.white,
                          backgroundColor: Colors.white,
                        );
                      },
                      child: FadeInY(0.2 * index, ItemView(items[index])),
                    );
                  }),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
