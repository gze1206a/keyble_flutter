import 'package:flutter/material.dart';
import 'package:keybleapp/providers.dart/network.dart';
import 'package:keybleapp/providers.dart/root.dart';
import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/screens/main/main1.dart';
import 'package:keybleapp/screens/main/main2.dart';
import 'package:keybleapp/screens/main/main3.dart';
import 'package:keybleapp/screens/main/main4.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:provider/provider.dart';

class MainScreen extends StatelessWidget {
  final controller = PageController();
  @override
  Widget build(BuildContext context) {
    final uiprovider = Provider.of<UIProvider>(context);

    final user = Provider.of<RootProvider>(context).user;
    print(user.accesstoken);
    Widget _buildButton(
        num index, String image, String title, VoidCallback event) {
      return GestureDetector(
        onTap: event,
        child: Opacity(
            opacity: uiprovider.navigationButtonIndex == index ? 1.0 : 0.5,
            child: Container(
              width: 64,
              // color: Colors.white,
              child: Column(
                children: <Widget>[
                  Image.asset(
                    uiprovider.navigationButtonIndex == index
                        ? image + ".png"
                        : image + "_d.png",
                    height: 42,
                  ),
                  Space(5),
                  TextG(
                    title,
                    size: 12,
                    type: TextType.Bold,
                  )
                ],
              ),
            )),
      );
    }

    return BaseScreenWithoutAppbar(
        body: Stack(
      children: <Widget>[
        PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: controller,
          children: <Widget>[
            Main1Screen(),
            Main2Screen(),
            Main3Screen(),
            Main4Screen(),
          ],
        ),
        //bottombar
        Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 18, vertical: 10),
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                          height: 76,
                          child: Card(
                              elevation: 4,
                              margin: EdgeInsets.zero,
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(40.0)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    _buildButton(
                                        0, "assets/images/icon_1", "HOME", () {
                                      uiprovider.navigationButtonIndex = 0;
                                      uiprovider.setTabIndex(true);
                                      controller.animateToPage(0,
                                          duration: Duration(
                                              milliseconds: 300 +
                                                  (controller.page - 0)
                                                          .abs()
                                                          .toInt() *
                                                      200),
                                          curve: Curves.easeInOut);
                                    }),
                                    _buildButton(
                                        1, "assets/images/icon_2", "SHOP",
                                        () async {
                                      uiprovider.navigationButtonIndex = 1;
                                      uiprovider.setItemTabIndex(0);
                                      controller.animateToPage(1,
                                          duration: Duration(
                                              milliseconds: 300 +
                                                  (controller.page - 1)
                                                          .abs()
                                                          .toInt() *
                                                      200),
                                          curve: Curves.easeInOut);
                                      uiprovider.onLoad();
                                      await Provider.of<NetworkProvider>(
                                              context,
                                              listen: false)
                                          .loadItems(user.accesstoken);
                                      uiprovider.onHide();
                                    }),
                                    Space(60),
                                    _buildButton(
                                        2, "assets/images/icon_3", "Message",
                                        () async {
                                      uiprovider.navigationButtonIndex = 2;
                                      uiprovider.setMyTabIndex(true);
                                      controller.animateToPage(2,
                                          duration: Duration(
                                              milliseconds: 300 +
                                                  (controller.page - 2)
                                                          .abs()
                                                          .toInt() *
                                                      200),
                                          curve: Curves.easeInOut);
                                      uiprovider.onLoad();
                                      await Provider.of<NetworkProvider>(
                                              context,
                                              listen: false)
                                          .loadNotifications(user.accesstoken);
                                      uiprovider.onHide();
                                    }),
                                    _buildButton(
                                        3, "assets/images/icon_4", "MY",
                                        () async {
                                      uiprovider.setNaviIndex(3);
                                      controller.animateToPage(3,
                                          duration: Duration(
                                              milliseconds: 300 +
                                                  (controller.page - 3)
                                                          .abs()
                                                          .toInt() *
                                                      200),
                                          curve: Curves.easeInOut);
                                      uiprovider.onLoad();
                                      await Provider.of<NetworkProvider>(
                                              context,
                                              listen: false)
                                          .loadCollections(
                                              user.id, user.accesstoken);
                                      await Provider.of<NetworkProvider>(
                                              context,
                                              listen: false)
                                          .loadPosts(user.id, user.accesstoken);
                                      uiprovider.onHide();
                                    }),
                                  ],
                                ),
                              ))),
                    ),
                    Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 20),
                          child: Image.asset(
                            "assets/images/icon_mainbutton.png",
                            height: 72,
                          ),
                        ))
                  ],
                )))
      ],
    ));
  }
}
