import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/translations.dart';

class MyItemScreen extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyItemScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  UIProvider uiprovider;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 4);
    _tabController.addListener(() {
      uiprovider.setMyItemTabIndex(_tabController.index); // setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    uiprovider = Provider.of<UIProvider>(context);
    Widget _buildTitleView(num index, String title) {
      return Padding(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: TextG(
          title,
          size: 14,
          color: uiprovider.myItemTabIndex == index
              ? Colors.black
              : Color(0xffdcdcdc),
          type: TextType.Bold,
        ),
      );
    }

    return BaseScreen(
      useLoading: false,
      appbar: AppBar(
        backgroundColor: Colors.black,
        title: TextG(
          // Translations.of(context).trans('EmailLogin'),
          "My Item",
          size: 16,
          color: Colors.white,
          type: TextType.Bold,
        ),
        leading: Padding(
          padding: EdgeInsets.only(left: 4),
          child: Container(
              color: Colors.black,
              padding: EdgeInsets.all(16),
              child: Image.asset(
                "assets/images/icon_menu.png",
              )),
        ),
        actions: <Widget>[
          Container(
              color: Colors.black,
              padding: EdgeInsets.all(20),
              child: Image.asset(
                "assets/images/icon_filter.png",
              )),
        ],
        centerTitle: true,
        automaticallyImplyLeading: false,
        elevation: 0,
      ),
      body: Column(
        children: <Widget>[
          Divider(
            height: 1,
            thickness: 1,
            color: Color(0xff000000).withOpacity(0.9),
          ),
          Container(
              color: Colors.black,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 24),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      TextG(
                        Translations.of(context).trans('Mycoin'),
                        size: 14,
                        color: Colors.white,
                        type: TextType.Bold,
                      ),
                      ExpandedSpace(),
                      TextG(
                        "4,400",
                        size: 16,
                        color: Colors.white,
                        type: TextType.Bold,
                      ),
                      Space(20)
                    ],
                  ),
                ],
              )),
          TabBar(
            indicatorColor: Colors.black,
            controller: _tabController,
            tabs: [
              _buildTitleView(
                0,
                "Trace",
              ),
              _buildTitleView(1, "BGM"),
              _buildTitleView(2, "World"),
            ],
          ),
          Divider(
            color: Color(0xffdcdcdc),
            height: 1,
          ),
        ],
      ),
    );
  }
}
