import 'package:flutter/material.dart';
import 'package:keybleapp/models/item.dart';
import 'package:keybleapp/providers.dart/root.dart';
import 'package:keybleapp/screens/itemshop/itemdetail.dart';
import 'package:keybleapp/screens/itemshop/vipdetail.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/dialog.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/translations.dart';

class Main2Screen extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Main2Screen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  UIProvider uiprovider;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 4);
    _tabController.addListener(() {
      uiprovider.setItemTabIndex(_tabController.index); // setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final items = Provider.of<RootProvider>(context).items;
    uiprovider = Provider.of<UIProvider>(context);

    Widget _buildTitleView(num index, String title) {
      return Padding(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: TextG(
          title,
          size: 14,
          color: uiprovider.itemShopTabIndex == index
              ? Colors.black
              : Color(0xffdcdcdc),
          type: TextType.Bold,
        ),
      );
    }

    Widget _buildItemView(Item item) {
      return GestureDetector(
        onTap: () {
          uiprovider.selectItem = item;
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ItemDetailPage()));
        },
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
          child: Row(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                      width: 70.0,
                      height: 70.0,
                      decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.05),
                          borderRadius: BorderRadius.all(
                            Radius.circular(16),
                          ))),
                  Positioned(
                    left: 7.5,
                    top: 7.5,
                    child: Image.network(
                      item.image,
                      width: 55,
                      height: 55,
                    ),
                  )
                ],
              ),
              Space(16),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  FMButton(
                    "Best",
                    () {},
                    fontcolor: Colors.black,
                    bgcolor: Color(0xff00ffd4),
                  ),
                  Space(8),
                  TextG(
                    item.name,
                    size: 14,
                    color: Colors.black,
                    type: TextType.Bold,
                  ),
                  Space(8),
                  TextG(
                    item.name,
                    size: 12,
                    color: Color(0xff919191),
                  ),
                ],
              ),
              ExpandedSpace(),
              Container(
                width: 80,
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      "assets/images/icon_coin.png",
                      height: 16,
                    ),
                    Space(6),
                    TextG(
                      "${item.price}",
                      size: 14,
                      color: Colors.black,
                      type: TextType.Bold,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
    }

    return BaseScreen(
      useLoading: false,
      appbar: AppBar(
        backgroundColor: Colors.black,
        title: TextG(
          // Translations.of(context).trans('EmailLogin'),
          "Item Shop",
          size: 16,
          color: Colors.white,
          type: TextType.Bold,
        ),
        leading: Padding(
          padding: EdgeInsets.only(left: 4),
          child: Container(
              color: Colors.black,
              padding: EdgeInsets.all(16),
              child: Image.asset(
                "assets/images/icon_menu.png",
              )),
        ),
        actions: <Widget>[
          GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return ChoiceDialog("Filter setting", "Recent Uploaded",
                          "Best Selling", () {}, () {});
                    });
              },
              child: Container(
                  color: Colors.black,
                  padding: EdgeInsets.all(20),
                  child: Image.asset(
                    "assets/images/icon_filter.png",
                  ))),
        ],
        centerTitle: true,
        automaticallyImplyLeading: false,
        elevation: 0,
      ),
      body: Column(
        children: <Widget>[
          Divider(
            height: 1,
            thickness: 1,
            color: Color(0xff000000).withOpacity(0.9),
          ),
          Container(
              color: Colors.black,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 24),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      TextG(
                        Translations.of(context).trans('Mycoin'),
                        size: 14,
                        color: Colors.white,
                        type: TextType.Bold,
                      ),
                      ExpandedSpace(),
                      TextG(
                        "4,400",
                        size: 16,
                        color: Colors.white,
                        type: TextType.Bold,
                      ),
                      Space(20)
                    ],
                  ),
                ],
              )),
          TabBar(
            indicatorColor: Colors.black,
            controller: _tabController,
            tabs: [
              _buildTitleView(
                0,
                "Trace",
              ),
              _buildTitleView(1, "BGM"),
              _buildTitleView(2, "World"),
              _buildTitleView(3, "VIP"),
            ],
          ),
          Divider(
            color: Color(0xffdcdcdc),
            height: 1,
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: <Widget>[
                ListView.separated(
                  itemCount:
                      items.where((item) => item.category == "trace").length,
                  itemBuilder: (BuildContext context, int index) {
                    return _buildItemView(items
                        .where((item) => item.category == "trace")
                        .toList()[index]);
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return Divider(
                        thickness: 1,
                        height: 1,
                        color: Color(0xff9b9b9b).withOpacity(0.1));
                  },
                ),
                ListView.separated(
                  itemCount:
                      items.where((item) => item.category == "bgm").length,
                  itemBuilder: (BuildContext context, int index) {
                    return _buildItemView(items
                        .where((item) => item.category == "bgm")
                        .toList()[index]);
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return Divider(
                        thickness: 1,
                        height: 1,
                        color: Color(0xff9b9b9b).withOpacity(0.1));
                  },
                ),
                ListView.separated(
                  itemCount:
                      items.where((item) => item.category == "world").length,
                  itemBuilder: (BuildContext context, int index) {
                    return _buildItemView(items
                        .where((item) => item.category == "world")
                        .toList()[index]);
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return Divider(
                        thickness: 1,
                        height: 1,
                        color: Color(0xff9b9b9b).withOpacity(0.1));
                  },
                ),
                ListView(
                  children: <Widget>[
                    Space(40),
                    TextG(
                      "By upgrading to a VIP membership,\nyou can enjoy various benefits for free.",
                      size: 14,
                      height: 1.6,
                      type: TextType.Bold,
                    ),
                    Space(22),
                    Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => VipDetailPage()));
                          },
                          child: Image.asset(
                            "assets/images/image_vip.png",
                            height: 170,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              top: 20,
                              left: MediaQuery.of(context).size.width * 0.3),
                          child: TextG(
                            "\$ ${items.firstWhere((item) => item.category == "vip").price} / 1 Month",
                            size: 14,
                            color: Colors.white,
                            type: TextType.Bold,
                          ),
                        ),
                      ],
                    ),
                    Space(40),
                    Row(
                      children: <Widget>[
                        Space(30),
                        TextG(
                          "VIP Benefits",
                          size: 16,
                          type: TextType.Bold,
                        ),
                      ],
                    ),
                    Space(8),
                    DotText("Free enterance to Keyble world"),
                    DotText("All Free trace item provided"),
                    DotText("Unlimited Video Call"),
                    Space(160),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
