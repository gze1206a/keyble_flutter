import 'package:flutter/material.dart';
import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:provider/provider.dart';

import 'package:keybleapp/animation/fadein.dart';
import 'package:keybleapp/providers.dart/root.dart';
import 'package:keybleapp/widgets/const.dart';

class Main3Screen extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Main3Screen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  UIProvider uiprovider;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      uiprovider.setMyTabIndex(_tabController.index == 0); // setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final notifys = Provider.of<RootProvider>(context).notifications;
    uiprovider = Provider.of<UIProvider>(context);
    Widget _buildNotify(num index) {
      return FadeInY(
          index * 0.2,
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Row(
              children: <Widget>[
                Space(20),
                notifys[index].photoThumb != null
                    ? Image.network(
                        notifys[index].photoThumb,
                        height: 48,
                      )
                    : Image.asset(
                        "assets/images/green.png",
                        height: 48,
                      ),
                Space(8),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextG(
                      notifys[index].message,
                      size: 14,
                    ),
                    Space(8),
                    TextG(
                      notifys[index].createdAt,
                      size: 12,
                      color: border_gray,
                    ),
                  ],
                ),
                ExpandedSpace(),
              ],
            ),
          ));
    }

    return BaseScreenWithoutAppbar(
        useLoading: false,
        body: Column(
          children: <Widget>[
            Space(36),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.2),
              child: TabBar(
                labelPadding: EdgeInsets.zero,
                indicatorPadding: EdgeInsets.zero,
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    color: Colors.black),
                controller: _tabController,
                tabs: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(12, 10, 12, 8),
                    child: TextG(
                      "Activity",
                      size: uiprovider.locale == "ko_KR" ? 14 : 12,
                      color:
                          uiprovider.myTabIndex ? Colors.white : Colors.black,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(12, 10, 12, 8),
                    child: TextG(
                      "Message",
                      size: uiprovider.locale == "ko_KR" ? 14 : 12,
                      color:
                          uiprovider.myTabIndex ? Colors.black : Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _tabController,
                  children: [
                    notifys == null || notifys.length == 0
                        ? Container()
                        : ListView.builder(
                            physics: BouncingScrollPhysics(),
                            itemCount: notifys.length,
                            itemBuilder: (BuildContext context, int index) {
                              return _buildNotify(index);
                            },
                          ),
                    Container(),
                  ]),
            )
          ],
        ));
  }
}
