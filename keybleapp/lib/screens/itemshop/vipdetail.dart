import 'package:flutter/material.dart';
import 'package:keybleapp/providers.dart/root.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:provider/provider.dart';

class VipDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final items = Provider.of<RootProvider>(context).objectitems;

    return BaseScreen(
      title: "Membership detail",
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                Space(40),
                Image.asset(
                  "assets/images/image_vip.png",
                  height: 170,
                ),
                Space(30),
                TextG(
                  "By upgrading to a VIP membership,\nyou can enjoy various benefits for free.",
                  size: 14,
                  height: 1.6,
                  type: TextType.Bold,
                ),
                Space(16),
                TextG(
                  "\$7.00 / 1 Month",
                  size: 12,
                  color: Color(0xff919191),
                ),
                Padding(
                    padding: EdgeInsets.symmetric(vertical: 30),
                    child: Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Space(10),
                          )
                        ],
                      ),
                      color: Color(0xfff4f4f4),
                    )),
                Row(
                  children: <Widget>[
                    Space(30),
                    TextG(
                      "Check This",
                      size: 16,
                      type: TextType.Bold,
                    ),
                  ],
                ),
                Space(8),
                DotText(
                    "Coin is the unit of currency, used to purchase keyble items."),
                DotText(
                    "Purchase holds on Payment Service of Each Application Stores, Google Play and App Store."),
                DotText(
                    "Customer who wants to refund purchases, needs to contact CS center of KEYBLE. After authenification, refund holds in 3days."),
                DotText("All Prices of product contains VAT check."),
                Space(12),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 30),
            child: BottomButton("Check Out", () {}),
          )
        ],
      ),
    );
  }
}
