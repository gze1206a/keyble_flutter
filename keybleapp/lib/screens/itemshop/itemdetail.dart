import 'package:flutter/material.dart';
import 'package:keybleapp/providers.dart/root.dart';
import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/widgets/base_scaffold.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/text.dart';
import 'package:keybleapp/widgets/translations.dart';
import 'package:provider/provider.dart';

class ItemDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<RootProvider>(context).user;
    final uiprovider = Provider.of<UIProvider>(context);

    return BaseScreen(
      title: "Membership detail",
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                    child: Card(
                        elevation: 0,
                        color: Colors.white,
                        shape: StadiumBorder(
                            side: BorderSide(color: Colors.black, width: 1.0)),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 16, horizontal: 30),
                          child: Row(
                            children: <Widget>[
                              TextG(
                                Translations.of(context).trans('Mycoin'),
                                size: 12,
                                type: TextType.Bold,
                              ),
                              ExpandedSpace(),
                              TextG(
                                "${user.point}",
                                size: 16,
                                type: TextType.Bold,
                              ),
                            ],
                          ),
                        )),
                  ),
                ),
                Space(40),
                Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Container(
                        width: 100.0,
                        height: 100.0,
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.05),
                            borderRadius: BorderRadius.all(
                              Radius.circular(16),
                            ))),
                    Image.network(
                      uiprovider.selectItem.image,
                      width: 55,
                      height: 55,
                    )
                  ],
                ),
                Space(30),
                TextG(
                  uiprovider.selectItem.name,
                  size: 14,
                  height: 1.6,
                  type: TextType.Bold,
                ),
                Space(16),
                TextG(
                  "\$ ${uiprovider.selectItem.price}",
                  size: 12,
                  color: Color(0xff919191),
                ),
                Padding(
                    padding: EdgeInsets.symmetric(vertical: 30),
                    child: Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Space(10),
                          )
                        ],
                      ),
                      color: Color(0xfff4f4f4),
                    )),
                Row(
                  children: <Widget>[
                    Space(30),
                    TextG(
                      "Check This",
                      size: 16,
                      type: TextType.Bold,
                    ),
                  ],
                ),
                Space(8),
                DotText(
                    "Coin is the unit of currency, used to purchase keyble items."),
                DotText(
                    "Purchase holds on Payment Service of Each Application Stores, Google Play and App Store."),
                DotText(
                    "Customer who wants to refund purchases, needs to contact CS center of KEYBLE. After authenification, refund holds in 3days."),
                DotText("All Prices of product contains VAT check."),
                Space(12),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 30),
            child: BottomButton("Check Out", () {}),
          )
        ],
      ),
    );
  }
}
