import 'package:flutter/material.dart';
import 'package:keybleapp/widgets/button.dart';
import 'package:keybleapp/widgets/const.dart';
import 'package:keybleapp/widgets/dialog.dart';

class UIKit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget getLoginButton() {
      return GestureDetector(
        onTap: () {},
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 40, vertical: 0),
            child: Card(
                elevation: 0,
                color: Colors.white,
                shape: StadiumBorder(
                    side: BorderSide(color: Colors.white, width: 1.0)),
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "아이디 로그인",
                      )
                    ],
                  ),
                )),
          ),
        ),
      );
    }
//flutter_custom_dialog: ^1.0.19
    // YYDialog YYDialogDemo(BuildContext context) {
    //   return YYDialog().build(context)
    //     ..width = 220
    //     ..height = 500
    //     ..widget(
    //       Padding(
    //         padding: EdgeInsets.all(0.0),
    //         child: Align(
    //           alignment: Alignment.centerLeft,
    //           child: Text(
    //             "123",
    //             style: TextStyle(
    //               color: Colors.black,
    //               fontSize: 14.0,
    //               fontWeight: FontWeight.w100,
    //             ),
    //           ),
    //         ),
    //       ),
    //     )
    //     ..show();
    // }

    return Scaffold(
      body: Column(
        children: <Widget>[
          Space(24),
          getLoginButton(),
          Space(24),
          OutLMButton("Shoes", () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return KeyDialog(
                      "3", "The key has been used\nsusccessfully!!");
                });
          }),
          Space(24),
          OutLButton("#Cloth", () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return DoubleTextDialog("Certified mail has been sent.",
                      "The key has been used\nsuccessfully!!");
                });
          }),
          Space(24),
          FButton("#Cloth", () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return TextDialog("The key has been used\nsuccessfully!!");
                });
          }),
          Space(24),
          OutLButton("OutLButton", () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return PeopleDialog();
                });
          }),
          OutLButton("OutLButton", () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return MaskDialog();
                });
          }),
        ],
      ),
    );
  }
}
