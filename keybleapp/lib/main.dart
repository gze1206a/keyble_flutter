import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:keybleapp/providers.dart/network.dart';
import 'package:keybleapp/providers.dart/root.dart';
import 'package:keybleapp/providers.dart/theme.dart';
import 'package:keybleapp/providers.dart/ui.dart';
import 'package:keybleapp/screens/login/splash.dart';
import 'package:keybleapp/widgets/translations_delegate.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  //상단바 및 하단바 색상 조정
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarIconBrightness: Brightness.dark,
      statusBarColor: Colors.transparent));

  //디바이스 세로, 가로 고정
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(LaunchApp());
  });
}

class LaunchApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // light, dark theme
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeProvider>(create: (_) => ThemeProvider()),
        ChangeNotifierProvider<RootProvider>(create: (_) => RootProvider()),
        ChangeNotifierProvider<UIProvider>(create: (_) => UIProvider()),
        ProxyProvider<RootProvider, NetworkProvider>(
          update: (BuildContext context, RootProvider root,
                  NetworkProvider previous) =>
              NetworkProvider(root),
        ),
      ],
      child: InitPage(),
    );
  }
}

class InitPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Provider.of<UIProvider>(context).init();
    return MaterialApp(
      title: 'Flutter Skeleton',
      theme: Provider.of<ThemeProvider>(context).getTheme(),
      supportedLocales: [
        const Locale('ko', 'KR'),
        const Locale('en', 'US'),
        const Locale('ja', 'JP')
      ],
      localizationsDelegates: [
        const TranslationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      localeResolutionCallback:
          (Locale locale, Iterable<Locale> supportedLocales) {
        if (locale == null) {
          debugPrint("*language locale is null!!!");
          return supportedLocales.first;
        }

        for (Locale supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale.languageCode ||
              supportedLocale.countryCode == locale.countryCode) {
            debugPrint("*language ok $supportedLocale");
            return supportedLocale;
          }
        }

        debugPrint("*language to fallback ${supportedLocales.first}");
        return supportedLocales.first;
      },
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}
