import 'package:flutter/material.dart';
import 'package:keybleapp/models/item.dart';
import 'package:keybleapp/viewmodels/example.dart';
import 'package:keybleapp/viewmodels/join.dart';

class UIProvider with ChangeNotifier {
  String locale;
  bool isLoading = false;
  onLoad() {
    isLoading = true;
    refresh();
  }

  onHide() {
    isLoading = false;
    refresh();
  }

  //JOIN
  Join join;
  PageController controller;
  //MainButtonIndex
  num navigationButtonIndex = 0;
  setNaviIndex(num res) {
    navigationButtonIndex = res;
    refresh();
  }

  //Main
  bool mainTabIndex = true;
  setTabIndex(bool res) {
    mainTabIndex = res;
    refresh();
  }

  //Item
  num itemShopTabIndex = 0;
  setItemTabIndex(num res) {
    itemShopTabIndex = res;
    refresh();
  }
  Item selectItem;
  //MyItem
  num myItemTabIndex = 0;
  setMyItemTabIndex(num res) {
    myItemTabIndex = res;
    refresh();
  }

  //Mypage
  bool myTabIndex = true;
  setMyTabIndex(bool res) {
    myTabIndex = res;
    refresh();
  }

  ExampleUI example;
  // init() {
  //   example = ExampleUI(this.refresh());
  // }

  refresh() {
    this.notifyListeners();
  }
}
