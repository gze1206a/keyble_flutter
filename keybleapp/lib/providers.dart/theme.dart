import 'package:flutter/material.dart';

class ThemeProvider with ChangeNotifier {
  final ThemeData dark = ThemeData(brightness: Brightness.dark);
  final ThemeData light = ThemeData(
      brightness: Brightness.light,
      primaryColor: Colors.white,
      accentColor: Colors.black);
  // accentColor: Color(0xff40bf7a),
  //   textTheme: TextTheme(
  //       title: TextStyle(color: Colors.black54),
  //       subtitle: TextStyle(color: Colors.grey),
  //       subhead: TextStyle(color: Colors.white)),
  //   appBarTheme: AppBarTheme(
  //       color: Color(0xff1f655d),
  //       actionsIconTheme: IconThemeData(color: Colors.white)
  ThemeData _theme;

  ThemeProvider() {
    _theme = light;
  }

  getTheme() => _theme;

  setTheme(ThemeData themeData) {
    _theme = themeData;
    notifyListeners();
  }

  switchTheme() {
    if (_theme == dark)
      _theme = light;
    else
      _theme = dark;
    notifyListeners();
  }
}
