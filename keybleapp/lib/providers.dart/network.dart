import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:keybleapp/models/base_object.dart';
import 'package:keybleapp/models/item.dart';
import 'package:keybleapp/models/notify.dart';
import 'package:keybleapp/models/object_item.dart';
import 'package:keybleapp/models/post.dart';
import 'package:keybleapp/models/user.dart';
import 'package:keybleapp/providers.dart/root.dart';

class NetworkProvider {
  final String baseurl = "http://tio.keyble.co.kr";
  RootProvider root;

  NetworkProvider(RootProvider context) : root = context;
  //사물 가져옥;
  //GET loadObjectItems
  Future<List> loadObjectItems(String token) async {
    var response = await http.get(baseurl + "/v2/translated/objectItems",
        headers: {"Authorization": "Bearer " + token});
    print(response.body);
    if (response.statusCode == 200) {
      List<ObjectItem> items = jsonDecode(response.body)
          .map<ObjectItem>((item) => ObjectItem.fromJson(item))
          .toList();
      root.objectitems = items;
      return items;
    } else
      return List();
  }

  //아이템샵
  //GET loadObjectItems
  Future<List> loadItems(String token) async {
    var response = await http.get(baseurl + "/itemshop",
        headers: {"Authorization": "Bearer " + token});
    print(response.body);
    if (response.statusCode == 200) {
      List<Item> items = jsonDecode(response.body)
          .map<Item>((item) => Item.fromJson(item))
          .toList();
      root.items = items;
      return items;
    } else
      return List();
  }

  //GET loadNotifications
  Future<List> loadNotifications(String token) async {
    var response = await http.get(baseurl + "/self/notifications",
        headers: {"Authorization": "Bearer " + token});
    print(response.body);
    if (response.statusCode == 200) {
      List<Notifiy> items = jsonDecode(response.body)
          .map<Notifiy>((item) => Notifiy.fromJson(item))
          .toList();
      root.notifications = items;
      return items;
    } else
      return List();
  }

  //GET loadCollections
  Future<List> loadCollections(num userId, String token) async {
    var response = await http.get(baseurl + "/users/$userId/keys",
        headers: {"Authorization": "Bearer " + token});
    print(response.body);
    if (response.statusCode == 200) {
      List<ObjectItem> collections = jsonDecode(response.body)
          .map<ObjectItem>((item) => ObjectItem.fromJson(item))
          .toList();
      root.collections = collections;
      return collections;
    } else
      return List();
  }

  //GET loadPosts
  Future<List> loadPosts(num userId, String token) async {
    var response = await http.get(baseurl + "/users/$userId/posts",
        headers: {"Authorization": "Bearer " + token});
    print(response.body);
    if (response.statusCode == 200) {
      List<UserPost> posts = jsonDecode(response.body)
          .map<UserPost>((item) => UserPost.fromJson(item))
          .toList();
      root.posts = posts;
      return posts;
    } else
      return List();
  }

  //Post Login
  Future<User> login(String id, String pw, String deviceType) async {
    var response = await http.post(baseurl + "/session", body: {
      "type": "local",
      "identifier": id,
      "data": pw,
      //"secret": "",
      "deviceType": deviceType
    });
    if (response.statusCode != 200)
      return User.empty(ResponseHeader.error(response));
    else {
      root.user = User.fromJson(jsonDecode(response.body));
      print(root.user.accesstoken);
      print(root.user.id);
      return root.user;
    }
  }

  // //POST - REGISTER
  // Future<AuthResult> registerUser(AuthUser user) async {
  //   var response = await http.post(baseurl + "/register", body: user.toJson());

  //   if (response.statusCode != 200)
  //     return AuthResult.empty(ResponseHeader.error(response));
  //   else
  //     return AuthResult.fromJson(jsonDecode(response.body));
  // }

  // //POST - CREATE / 201
  // Future<PeoPle> createUser(PeoPle people) async {
  //   var response = await http.post(baseurl + "/users", body: people.toJson());

  //   if (response.statusCode != 201)
  //     return PeoPle.empty(ResponseHeader.error(response));
  //   else
  //     return PeoPle.fromJson(jsonDecode(response.body));
  // }

  // //PUT - UPDATE
  // Future<PeoPle> putUser(PeoPle people) async {
  //   var response = await http.put(baseurl + "/users/2", body: people.toJson());

  //   if (response.statusCode != 200)
  //     return PeoPle.empty(ResponseHeader.error(response));
  //   else
  //     return PeoPle.fromJson(jsonDecode(response.body));
  // }

  // //PATCH - UPDATE
  // Future<PeoPle> patchUser(PeoPle people) async {
  //   var response =
  //       await http.patch(baseurl + "/users/2", body: people.toJson());

  //   if (response.statusCode != 200)
  //     return PeoPle.empty(ResponseHeader.error(response));
  //   else
  //     return PeoPle.fromJson(jsonDecode(response.body));
  // }

  // //DELETE
  // Future<PeoPle> deleteUser() async {
  //   var response = await http.delete(baseurl + "/users/2");

  //   if (response.statusCode != 204)
  //     return PeoPle.empty(ResponseHeader.error(response));
  //   else
  //     return PeoPle.empty(ResponseHeader.success());
  // }
}
