import 'package:flutter/material.dart';
import 'package:keybleapp/models/item.dart';
import 'package:keybleapp/models/notify.dart';
import 'package:keybleapp/models/object_item.dart';
import 'package:keybleapp/models/post.dart';
import 'package:keybleapp/models/user.dart';

class RootProvider with ChangeNotifier {
  User user;
  List<Item> items;
  List<ObjectItem> objectitems;
  List<ObjectItem> collections;
  List<UserPost> posts;
  List<Notifiy> notifications;
  setUser(User data) {
    user = data;
    notifyListeners();
  }
}
